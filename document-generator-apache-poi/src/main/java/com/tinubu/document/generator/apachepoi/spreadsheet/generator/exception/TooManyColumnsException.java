package com.tinubu.document.generator.apachepoi.spreadsheet.generator.exception;

import java.text.MessageFormat;

/**
 * Indicates that the total number of columns on an object is exceeded.
 */
public class TooManyColumnsException extends RuntimeException {
   private static final MessageFormat DEFAULT_MESSAGE =
         new MessageFormat("You provide {0} columns which exceeds {1} equal to maximum limit of columns");

   public TooManyColumnsException(String message) {
      super(message);
   }

   public TooManyColumnsException(int numberOfColumn, int maxLimit) {
      this(DEFAULT_MESSAGE.format(new Object[] { numberOfColumn, maxLimit }));
   }
}
