package com.tinubu.document.generator.core.generator;

import static com.tinubu.commons.ddd2.invariant.Invariant.validate;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;
import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.util.StreamUtils.stream;
import static com.tinubu.commons.lang.validation.Validate.notNull;
import static com.tinubu.document.generator.core.generator.GeneratedDocumentRepositoryFactory.autoExtension;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.function.Function;

import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.tinubu.commons.ddd2.criterion.Criterion.CriterionBuilder;
import com.tinubu.commons.ddd2.domain.ids.Uuid;
import com.tinubu.commons.ddd2.domain.type.AbstractValue;
import com.tinubu.commons.ddd2.domain.type.Fields;
import com.tinubu.commons.lang.beans.Getter;
import com.tinubu.commons.lang.mimetype.MimeType;
import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.document.domain.Document.DocumentBuilder;
import com.tinubu.commons.ports.document.domain.DocumentContent;
import com.tinubu.commons.ports.document.domain.DocumentEntryCriteria;
import com.tinubu.commons.ports.document.domain.DocumentEntryCriteria.DocumentEntryCriteriaBuilder;
import com.tinubu.commons.ports.document.domain.DocumentEntrySpecification;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.domain.DocumentRepository;
import com.tinubu.commons.ports.document.domain.processor.DocumentProcessor;
import com.tinubu.commons.ports.document.fs.FsDocumentConfig;
import com.tinubu.commons.ports.document.fs.FsDocumentConfig.FsDocumentConfigBuilder;
import com.tinubu.commons.ports.document.fs.FsDocumentRepository;
import com.tinubu.commons.ports.document.fs.storagestrategy.DirectFsStorageStrategy;
import com.tinubu.document.generator.core.environment.Environment;

/**
 * Base implementation for document generators
 */
public abstract class AbstractDocumentGenerator extends AbstractValue implements DocumentGenerator {

   private static final Logger log = LoggerFactory.getLogger(AbstractDocumentGenerator.class);

   private final Environment environment;

   protected AbstractDocumentGenerator(Environment environment) {
      this.environment = environment;
   }

   @Override
   public Fields<? extends AbstractDocumentGenerator> defineDomainFields() {
      return Fields
            .<AbstractDocumentGenerator>builder()
            .field("environment", v -> v.environment, isNotNull())
            .build();
   }

   /**
    * Environment used for document generation.
    *
    * @return environment used for document generation
    */
   @Getter
   public Environment environment() {
      return environment;
   }

   /**
    * Generates the output document from content. Output document is named after template name and random UUID
    * to ensure document uniqueness.
    *
    * @param templateId template identifier
    * @param content output document content
    * @param outputType output document type
    *
    * @return output document in generated document repository
    */
   protected Document outputDocument(DocumentPath templateId, DocumentContent content, MimeType outputType) {
      validate(templateId, "templateId", isNotNull()).orThrow();
      validate(content, "content", isNotNull()).orThrow();

      String documentName =
            FilenameUtils.getBaseName(templateId.stringValue()) + "-" + Uuid.newRandomUuid().value();

      return autoExtension(new DocumentBuilder()
                                 .documentId(DocumentPath.of(documentName))
                                 .optionalChain(nullable(outputType), DocumentBuilder::contentType)
                                 .content(content)
                                 .build());
   }

   /**
    * Creates a filesystem-based sandbox repository for document generators that do not support
    * {@link DocumentRepository} abstraction, but work only on filesystem.
    * Repository is automatically deleted on close.
    * <p>
    * Use {@link #createSandboxRepository(Path, DocumentRepository, DocumentEntrySpecification,
    * DocumentProcessor)} for more powerful document filtering.
    *
    * @param sandboxPath real local filesystem path for sandbox, should be a safe, temporary, directory
    * @param documentRepository source repository
    * @param documentIds documents to transfer to sandbox
    *
    * @return filesystem repository with all documents transferred
    */
   protected FsDocumentRepository createSandboxRepository(Path sandboxPath,
                                                          DocumentRepository documentRepository,
                                                          DocumentPath... documentIds) {
      notNull(sandboxPath, "sandboxPath");
      notNull(documentRepository, "documentRepository");
      notNull(documentIds, "documentIds");

      List<Path> documentPaths = list(stream(documentIds).map(DocumentPath::stringValue).map(Paths::get));
      return createSandboxRepository(sandboxPath,
                                     documentRepository,
                                     new DocumentEntryCriteriaBuilder()
                                           .documentPath(CriterionBuilder.in(documentPaths))
                                           .build(),
                                     null);
   }

   /**
    * Creates a filesystem-based sandbox repository for document generators that do not support
    * {@link DocumentRepository} abstraction.
    * Repository is automatically deleted on close.
    *
    * @param sandboxPath real local filesystem path for sandbox, should be a safe, temporary, directory
    * @param documentRepository source repository
    * @param documentSpecification documents to transfer to sandbox
    * @param documentProcessor optional document processor
    *
    * @return filesystem repository with all documents transferred
    *
    * @see DocumentEntryCriteria
    */
   protected FsDocumentRepository createSandboxRepository(Path sandboxPath,
                                                          DocumentRepository documentRepository,
                                                          DocumentEntrySpecification documentSpecification,
                                                          DocumentProcessor documentProcessor) {
      notNull(sandboxPath, "sandboxPath");
      notNull(documentRepository, "documentRepository");
      notNull(documentSpecification, "documentSpecification");

      FsDocumentConfig sandboxConfig = new FsDocumentConfigBuilder()
            .storagePath(sandboxPath)
            .storageStrategy(DirectFsStorageStrategy.class)
            .deleteRepositoryOnClose(true)
            .build();

      FsDocumentRepository sandbox = new FsDocumentRepository(sandboxConfig);
      try {
         documentRepository.transferDocuments(documentSpecification,
                                              sandbox,
                                              false,
                                              nullable(documentProcessor, Function.identity()));

         return sandbox;
      } catch (Exception e) {
         sandbox.close();
         throw e;
      }
   }

}
