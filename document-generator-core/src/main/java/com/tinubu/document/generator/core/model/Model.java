package com.tinubu.document.generator.core.model;

public interface Model {
   /**
    * No-op {@link Model} instance.
    *
    * @return no-op model
    */
   static NoopModel noopModel() {
      return new NoopModel();
   }

}
