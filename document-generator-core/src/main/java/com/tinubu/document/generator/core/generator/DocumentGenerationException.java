package com.tinubu.document.generator.core.generator;

/**
 * Generic error while generating document.
 */
public class DocumentGenerationException extends RuntimeException {
   public DocumentGenerationException(String message) {
      super(message);
   }

   public DocumentGenerationException(String message, Throwable cause) {
      super(message, cause);
   }
}
