package com.tinubu.document.generator.core.environment;

import java.nio.charset.Charset;
import java.time.ZoneId;
import java.util.Locale;

import com.tinubu.commons.ddd2.domain.type.AbstractValue;
import com.tinubu.commons.ddd2.domain.type.DomainBuilder;
import com.tinubu.commons.ddd2.domain.type.Fields;
import com.tinubu.commons.lang.beans.Getter;
import com.tinubu.commons.lang.beans.Setter;

/**
 * User-configurable environment.
 */
public class ConfigurableEnvironment extends AbstractValue implements Environment {

   protected final ZoneId timeZone;
   protected final Locale locale;
   protected final String dateFormat;
   protected final String timeFormat;
   protected final String dateTimeFormat;
   protected final Charset documentEncoding;

   protected ConfigurableEnvironment(ConfigurableEnvironmentBuilder builder) {
      this.timeZone = builder.timeZone;
      this.locale = builder.locale;
      this.dateFormat = builder.dateFormat;
      this.timeFormat = builder.timeFormat;
      this.dateTimeFormat = builder.dateTimeFormat;
      this.documentEncoding = builder.documentEncoding;
   }

   @Override
   @SuppressWarnings("unchecked")
   public Fields<? extends ConfigurableEnvironment> defineDomainFields() {
      return (Fields<ConfigurableEnvironment>) Environment.super.defineDomainFields();
   }

   @Getter
   public ZoneId timeZone() {
      return timeZone;
   }

   @Getter
   public Locale locale() {
      return locale;
   }

   @Getter
   public String dateFormat() {
      return dateFormat;
   }

   @Getter
   public String timeFormat() {
      return timeFormat;
   }

   @Getter
   public String dateTimeFormat() {
      return dateTimeFormat;
   }

   @Override
   public Charset documentEncoding() {
      return documentEncoding;
   }

   public static ConfigurableEnvironmentBuilder reconstituteBuilder() {
      return new ConfigurableEnvironmentBuilder().reconstitute();
   }

   public static class ConfigurableEnvironmentBuilder extends DomainBuilder<ConfigurableEnvironment> {

      private ZoneId timeZone;
      private Locale locale;
      private String dateFormat;
      private String timeFormat;
      private String dateTimeFormat;
      private Charset documentEncoding;

      public static ConfigurableEnvironmentBuilder ofEnvironment(Environment environment) {
         return new ConfigurableEnvironmentBuilder()
               .timeZone(environment.timeZone())
               .locale(environment.locale())
               .dateFormat(environment.dateFormat())
               .timeFormat(environment.timeFormat())
               .dateTimeFormat(environment.dateTimeFormat())
               .documentEncoding(environment.documentEncoding());
      }

      @Setter
      public ConfigurableEnvironmentBuilder timeZone(ZoneId timeZone) {
         this.timeZone = timeZone;
         return this;
      }

      @Setter
      public ConfigurableEnvironmentBuilder locale(Locale locale) {
         this.locale = locale;
         return this;
      }

      @Setter
      public ConfigurableEnvironmentBuilder dateFormat(String dateFormat) {
         this.dateFormat = dateFormat;
         return this;
      }

      @Setter
      public ConfigurableEnvironmentBuilder timeFormat(String timeFormat) {
         this.timeFormat = timeFormat;
         return this;
      }

      @Setter
      public ConfigurableEnvironmentBuilder dateTimeFormat(String dateTimeFormat) {
         this.dateTimeFormat = dateTimeFormat;
         return this;
      }

      @Setter
      public ConfigurableEnvironmentBuilder documentEncoding(Charset documentEncoding) {
         this.documentEncoding = documentEncoding;
         return this;
      }

      @Override
      protected ConfigurableEnvironment buildDomainObject() {
         return new ConfigurableEnvironment(this);
      }

   }
}
