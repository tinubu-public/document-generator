package com.tinubu.document.generator.core.generator;

import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.document.domain.processor.common.ContentTypeDocumentRenamer;

public class GeneratedDocumentRepositoryFactory {

   private static final ContentTypeDocumentRenamer CONTENT_TYPE_RENAMER_PROCESSOR =
         new ContentTypeDocumentRenamer(false, true);

   @SuppressWarnings("unchecked")
   public static <T extends Document> T autoExtension(T document) {
      return (T) document.process(CONTENT_TYPE_RENAMER_PROCESSOR);
   }
}
