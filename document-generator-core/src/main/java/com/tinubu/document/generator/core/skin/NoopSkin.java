package com.tinubu.document.generator.core.skin;

import static com.tinubu.commons.lang.util.OptionalUtils.optional;

import java.util.Optional;

import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.domain.DocumentRepository;

/**
 * Generic no-op skin definition.
 */
public class NoopSkin implements Skin {

   @Override
   public Optional<Document> skin() {
      return optional();
   }

   @Override
   public Optional<DocumentPath> logoId() {
      return optional();
   }

   @Override
   public Optional<DocumentPath> watermarkId() {
      return optional();
   }

   @Override
   public Optional<DocumentRepository> skinRepository() {
      return optional();
   }

}
