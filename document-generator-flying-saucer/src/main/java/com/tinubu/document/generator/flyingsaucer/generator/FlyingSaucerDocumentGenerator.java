package com.tinubu.document.generator.flyingsaucer.generator;

import static com.tinubu.commons.ddd2.invariant.Invariant.validate;
import static com.tinubu.commons.ddd2.invariant.ParameterValue.value;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;
import static com.tinubu.commons.ddd2.invariant.rules.ClassRules.isInstanceOf;
import static com.tinubu.commons.lang.io.PlaceholderReplacerReader.mapModelReplacementFunction;
import static com.tinubu.commons.lang.mimetype.registry.PresetMimeTypeRegistry.APPLICATION_PDF;
import static com.tinubu.commons.lang.mimetype.registry.PresetMimeTypeRegistry.APPLICATION_XHTML;
import static com.tinubu.commons.lang.mimetype.registry.PresetMimeTypeRegistry.TEXT_ANY;
import static com.tinubu.commons.lang.mimetype.registry.PresetMimeTypeRegistry.TEXT_CSS;
import static com.tinubu.commons.lang.util.StreamUtils.stream;
import static java.nio.charset.StandardCharsets.UTF_8;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import org.xhtmlrenderer.pdf.ITextRenderer;

import com.itextpdf.text.DocumentException;
import com.tinubu.commons.ddd2.domain.type.DomainBuilder;
import com.tinubu.commons.ddd2.domain.type.Fields;
import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.document.domain.DocumentContent;
import com.tinubu.commons.ports.document.domain.DocumentRepository;
import com.tinubu.commons.ports.document.domain.InputStreamDocumentContent.InputStreamDocumentContentBuilder;
import com.tinubu.commons.ports.document.domain.processor.DocumentProcessor;
import com.tinubu.commons.ports.document.domain.processor.replacer.PlaceholderReplacer;
import com.tinubu.document.generator.core.environment.Environment;
import com.tinubu.document.generator.core.environment.SystemEnvironment;
import com.tinubu.document.generator.core.generator.AbstractDocumentGenerator;
import com.tinubu.document.generator.core.generator.DocumentGenerationException;
import com.tinubu.document.generator.core.generator.GeneratedDocument;
import com.tinubu.document.generator.core.generator.GeneratedDocument.GeneratedDocumentBuilder;
import com.tinubu.document.generator.core.model.Model;
import com.tinubu.document.generator.core.model.TextModel;
import com.tinubu.document.generator.core.skin.NoopSkin;
import com.tinubu.document.generator.core.skin.Skin;
import com.tinubu.document.generator.core.template.Template;
import com.tinubu.document.generator.flyingsaucer.factory.B64ImgReplacedElementFactory;
import com.tinubu.document.generator.flyingsaucer.factory.ChainingReplacedElementFactory;
import com.tinubu.document.generator.flyingsaucer.factory.SVGReplacedElementFactory;
import com.tinubu.document.generator.type.html.template.XhtmlTemplate;
import com.tinubu.document.generator.type.html.template.XhtmlTemplate.XhtmlTemplateBuilder;

// FIXME header/footer ?
public class FlyingSaucerDocumentGenerator extends AbstractDocumentGenerator {

   protected FlyingSaucerDocumentGenerator(FlyingSaucerDocumentGeneratorBuilder builder) {
      super(builder.environment);
   }

   @Override
   @SuppressWarnings("unchecked")
   public Fields<? extends FlyingSaucerDocumentGenerator> defineDomainFields() {
      return Fields
            .<FlyingSaucerDocumentGenerator>builder()
            .superFields((Fields<FlyingSaucerDocumentGenerator>) super.defineDomainFields())
            .build();
   }

   @Override
   public GeneratedDocument generateFromTemplate(Template template, Model model, Skin skin) {
      validate(template, "template", isInstanceOf(value(XhtmlTemplate.class))).orThrow();
      validate(model, "model", isInstanceOf(value(TextModel.class))).orThrow();
      validate(skin, "skin", isInstanceOf(value(NoopSkin.class))).orThrow();

      try {
         return generatePdfDocument((XhtmlTemplate) template, (TextModel) model);
      } catch (Exception e) {
         throw new DocumentGenerationException(e.getMessage(), e);
      } finally {
         template.closeResources();
      }
   }

   @Override
   @SuppressWarnings("resource")
   public GeneratedDocument convert(GeneratedDocument generatedDocument) {
      validate(generatedDocument, "generatedDocument", isNotNull()).orThrow();

      try {
         return generatePdfDocument(XhtmlTemplateBuilder.ofGeneratedDocument(generatedDocument).build(),
                                    TextModel.noopModel());
      } catch (Exception e) {
         throw new DocumentGenerationException(e.getMessage(), e);
      }
   }

   /**
    * Generates PDF document from HTML document.
    *
    * @param template template
    *
    * @return Generated PDF document
    *
    * @throws DocumentGenerationException if an error occurs while generating the document
    */
   private GeneratedDocument generatePdfDocument(XhtmlTemplate template, TextModel model) {
      try (DocumentRepository autocloseTemplateRepository = template.templateRepository().orElse(null);
           ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
         Document templateDocument = template.template();
         ITextRenderer renderer = new ITextRenderer();

         DocumentRepositoryUserAgent documentUserAgent =
               documentRepositoryUserAgent(renderer, template, model);

         ChainingReplacedElementFactory chainingReplacedElementFactory = new ChainingReplacedElementFactory();
         chainingReplacedElementFactory.addReplacedElementFactory(new SVGReplacedElementFactory());
         chainingReplacedElementFactory.addReplacedElementFactory(new B64ImgReplacedElementFactory());

         renderer.getSharedContext().setUserAgentCallback(documentUserAgent);
         renderer.getSharedContext().setReplacedElementFactory(chainingReplacedElementFactory);
         renderer.setDocumentFromString(templateDocument.content().stringContent(UTF_8), null);
         renderer.layout();

         renderer.createPDF(outputStream);
         renderer.finishPDF();

         DocumentContent content =
               new InputStreamDocumentContentBuilder().content(outputStream.toByteArray(), UTF_8).build();

         return new GeneratedDocumentBuilder()
               .document(outputDocument(template.template().documentId(), content, APPLICATION_PDF))
               .build();
      } catch (IOException | DocumentException e) {
         throw new IllegalStateException(e);
      }
   }

   private DocumentRepositoryUserAgent documentRepositoryUserAgent(ITextRenderer renderer,
                                                                   XhtmlTemplate template,
                                                                   TextModel model) {
      return template.templateRepository().map(templateRepository -> {
         DocumentRepositoryUserAgent documentUserAgent =
               new DocumentRepositoryUserAgent(renderer.getOutputDevice(),
                                               templateRepository,
                                               placeholderReplacer(model));
         documentUserAgent.setSharedContext(renderer.getSharedContext());

         return documentUserAgent;
      }).orElse(null);
   }

   private DocumentProcessor placeholderReplacer(TextModel model) {
      return document -> {
         if (stream(TEXT_ANY, APPLICATION_XHTML, TEXT_CSS).anyMatch(replaceContentType -> document
               .metadata()
               .simpleContentType()
               .map(replaceContentType::matches)
               .orElse(false))) {
            return document.process(PlaceholderReplacer.of(mapModelReplacementFunction(model.data())));
         } else {
            return document;
         }
      };
   }

   public static class FlyingSaucerDocumentGeneratorBuilder
         extends DomainBuilder<FlyingSaucerDocumentGenerator> {
      private final Environment environment;

      public FlyingSaucerDocumentGeneratorBuilder(Environment environment) {
         this.environment = validate(environment, "environment", isNotNull()).orThrow();
      }

      public FlyingSaucerDocumentGeneratorBuilder() {
         this(SystemEnvironment.ofSystemDefaults());
      }

      @Override
      public FlyingSaucerDocumentGenerator buildDomainObject() {
         return new FlyingSaucerDocumentGenerator(this);
      }
   }

}
