package com.tinubu.document.generator.freemarker.generator;

import static com.tinubu.commons.ddd2.invariant.Invariant.validate;
import static com.tinubu.commons.ddd2.invariant.ParameterValue.value;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;
import static com.tinubu.commons.ddd2.invariant.rules.ClassRules.isInstanceOf;
import static com.tinubu.commons.lang.io.PlaceholderReplacerReader.mapModelReplacementFunction;
import static com.tinubu.commons.lang.mimetype.registry.PresetMimeTypeRegistry.TEXT_CSS;
import static com.tinubu.commons.lang.util.CollectionUtils.listConcat;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.util.StreamUtils.stream;

import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;
import java.util.function.BiFunction;

import com.tinubu.commons.ddd2.domain.type.DomainBuilder;
import com.tinubu.commons.ddd2.domain.type.Fields;
import com.tinubu.commons.lang.beans.Setter;
import com.tinubu.commons.lang.mimetype.MimeType;
import com.tinubu.commons.lang.mimetype.MimeTypeFactory;
import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.document.domain.DocumentRepository;
import com.tinubu.commons.ports.document.domain.InputStreamDocumentContent;
import com.tinubu.commons.ports.document.domain.InputStreamDocumentContent.InputStreamDocumentContentBuilder;
import com.tinubu.commons.ports.document.domain.processor.DocumentProcessor;
import com.tinubu.commons.ports.document.domain.processor.replacer.PlaceholderReplacer;
import com.tinubu.commons.ports.document.domain.repository.UnionDocumentRepository;
import com.tinubu.document.generator.core.environment.Environment;
import com.tinubu.document.generator.core.environment.SystemEnvironment;
import com.tinubu.document.generator.core.generator.AbstractDocumentGenerator;
import com.tinubu.document.generator.core.generator.DocumentGenerationException;
import com.tinubu.document.generator.core.generator.GeneratedDocument;
import com.tinubu.document.generator.core.generator.GeneratedDocument.GeneratedDocumentBuilder;
import com.tinubu.document.generator.core.model.Model;
import com.tinubu.document.generator.core.model.TextModel;
import com.tinubu.document.generator.core.model.TextModel.TextModelBuilder;
import com.tinubu.document.generator.core.skin.Skin;
import com.tinubu.document.generator.freemarker.generator.FreemarkerConfiguration.FreemarkerConfigurer;
import com.tinubu.document.generator.freemarker.template.FtlTemplate;
import com.tinubu.document.generator.freemarker.template.FtlTemplate.FtlTemplateBuilder;

import freemarker.cache.MultiTemplateLoader;
import freemarker.cache.TemplateLoader;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;

public class FreemarkerDocumentGenerator extends AbstractDocumentGenerator {

   private final FreemarkerConfiguration freemarkerConfiguration;

   protected FreemarkerDocumentGenerator(FreemarkerDocumentGeneratorBuilder builder) {
      super(builder.environment);
      this.freemarkerConfiguration = new FreemarkerConfiguration(builder.environment);
   }

   @Override
   @SuppressWarnings("unchecked")
   public Fields<? extends FreemarkerDocumentGenerator> defineDomainFields() {
      return Fields
            .<FreemarkerDocumentGenerator>builder()
            .superFields((Fields<FreemarkerDocumentGenerator>) super.defineDomainFields())
            .field("configuration", v -> v.freemarkerConfiguration)
            .build();
   }

   @Override
   public GeneratedDocument generateFromTemplate(com.tinubu.document.generator.core.template.Template template,
                                                 Model model,
                                                 Skin skin) {
      validate(template, "template", isInstanceOf(value(FtlTemplate.class))).orThrow();
      validate(model, "model", isInstanceOf(value(TextModel.class))).orThrow();
      validate(skin, "skin", isInstanceOf(value(Skin.class))).orThrow();

      try {
         return generateDocument((FtlTemplate) template, (TextModel) model, skin);
      } catch (Exception e) {
         throw new DocumentGenerationException(e.getMessage(), e);
      } finally {
         template.closeResources();
      }
   }

   @Override
   @SuppressWarnings("resource")
   public GeneratedDocument convert(GeneratedDocument generatedDocument) {
      validate(generatedDocument, "generatedDocument", isNotNull()).orThrow();

      try {
         return generateDocument(FtlTemplateBuilder.ofGeneratedDocument(generatedDocument).build(),
                                 TextModel.noopModel(),
                                 Skin.noopSkin());
      } catch (Exception e) {
         throw new DocumentGenerationException(e.getMessage(), e);
      }
   }

   /**
    * Generates HTML document using Freemarker.
    *
    * @param template template
    * @param model model
    * @param skin skin
    *
    * @return Generated HTML document
    *
    * @throws DocumentGenerationException if an error occurs while generating the document
    */
   private GeneratedDocument generateDocument(FtlTemplate template, TextModel model, Skin skin) {
      try {

         FreemarkerConfiguration fmkConfiguration = new FreemarkerConfiguration(this.freemarkerConfiguration);

         TextModel ftlModel = addDocumentReferencesToModel(model, template, skin);

         fmkConfiguration.mergeSettings(configureTemplateLoader(template, ftlModel, skin));
         fmkConfiguration.configureOutputFormat(template.templateContentType().orElse(null));

         Configuration configuration = fmkConfiguration.configuration();

         Charset outputDocumentEncoding = Charset.forName(configuration.getDefaultEncoding());

         try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
              BufferedWriter outputWriter = new BufferedWriter(new OutputStreamWriter(outputStream,
                                                                                      outputDocumentEncoding))) {
            Template ftlTemplate = configuration.getTemplate(template.template().documentId().stringValue());

            ftlTemplate.process(ftlModel.data(), outputWriter);
            outputWriter.flush();

            MimeType outputContentType = template
                  .templateContentType()
                  .orElseGet(() -> nullable(ftlTemplate.getOutputFormat().getMimeType())
                        .map(MimeTypeFactory::parseMimeType)
                        .orElse(null));

            InputStreamDocumentContent content = new InputStreamDocumentContentBuilder()
                  .content(outputStream.toByteArray(), outputDocumentEncoding)
                  .build();

            return new GeneratedDocumentBuilder()
                  .document(outputDocument(template.template().documentId(), content, outputContentType))
                  .documentRepository(freemarkerDocumentRepository(template, skin))
                  .build();
         }
      } catch (IOException | TemplateException e) {
         throw new DocumentGenerationException(e.getMessage(), e);
      }
   }

   /**
    * Configures Freemarker template loader with template and skin {@link DocumentRepository repositories}.
    * If a user-specified loader is already set, the stack loaders, with the user-specified loader in
    * priority order.
    *
    * @param template template
    * @param skin skin
    *
    * @return Freemarker configurer
    */
   private FreemarkerConfigurer configureTemplateLoader(FtlTemplate template, TextModel model, Skin skin) {
      return configuration -> {
         TemplateLoader templateLoader = new MultiTemplateLoader(new TemplateLoader[] {
               new DocumentsTemplateLoader(listConcat(stream(template.template()),
                                                      stream(template.header()),
                                                      stream(template.footer()),
                                                      stream(skin.skin()))),
               new DocumentRepositoryTemplateLoader(FreemarkerDocumentGenerator.this.freemarkerDocumentRepository(
                     template,
                     skin), FreemarkerDocumentGenerator.this.cssPlaceholderReplacer(model)) });

         TemplateLoader currentTemplateLoader = configuration.getTemplateLoader();

         if (currentTemplateLoader == null) {
            configuration.setTemplateLoader(templateLoader);
         } else {
            configuration.setTemplateLoader(new MultiTemplateLoader(new TemplateLoader[] {
                  currentTemplateLoader, templateLoader }));
         }
      };
   }

   private DocumentProcessor cssPlaceholderReplacer(TextModel model) {
      return document -> {
         if (stream(TEXT_CSS).anyMatch(replaceContentType -> document
               .metadata()
               .simpleContentType()
               .map(replaceContentType::matches)
               .orElse(false))) {
            return document.process(PlaceholderReplacer.of(mapModelReplacementFunction(model.data())));
         } else {
            return document;
         }
      };
   }

   private TextModel addDocumentReferencesToModel(TextModel model, FtlTemplate template, Skin skin) {
      return TextModelBuilder
            .from(model)
            .addData("templateId", template.template().documentId())
            .optionalChain(template.header(), addDocument("headerId"))
            .optionalChain(template.footer(), addDocument("footerId"))
            .optionalChain(skin.skin(), addDocument("skinId"))
            .optionalChain(skin.logoId(), addData("logoId"))
            .optionalChain(skin.watermarkId(), addData("watermarkId"))
            .build();
   }

   /**
    * Returns Freemarker document repository for includes.
    *
    * @param template template document repository
    * @param skin skin document repository
    *
    * @return freemarker document repository
    *
    * @implSpec Skin repository is on top layer to override default skins provided with
    *       templates.
    */
   private DocumentRepository freemarkerDocumentRepository(FtlTemplate template, Skin skin) {
      return UnionDocumentRepository.ofLayers(listConcat(stream(template.templateRepository()),
                                                         stream(skin.skinRepository())));
   }

   private <U> BiFunction<TextModelBuilder, U, TextModelBuilder> addData(String dataKey) {
      return (builder, object) -> builder.addData(dataKey, object);
   }

   private <D extends Document> BiFunction<TextModelBuilder, D, TextModelBuilder> addDocument(String dataKey) {
      return (builder, document) -> builder.addData(dataKey, document.documentId());
   }

   public static class FreemarkerDocumentGeneratorBuilder extends DomainBuilder<FreemarkerDocumentGenerator> {
      private final Environment environment;
      private FreemarkerConfiguration freemarkerConfiguration;

      public FreemarkerDocumentGeneratorBuilder(Environment environment) {
         this.environment = validate(environment, "environment", isNotNull()).orThrow();
         this.freemarkerConfiguration = new FreemarkerConfiguration(environment);
      }

      public FreemarkerDocumentGeneratorBuilder() {
         this(SystemEnvironment.ofSystemDefaults());
      }

      @Setter
      public FreemarkerDocumentGeneratorBuilder freemarkerConfiguration(FreemarkerConfiguration freemarkerConfiguration) {
         this.freemarkerConfiguration =
               nullable(freemarkerConfiguration, () -> new FreemarkerConfiguration(environment));
         return this;
      }

      public FreemarkerDocumentGeneratorBuilder mergeSettings(FreemarkerConfigurer config) {
         freemarkerConfiguration.mergeSettings(config);
         return this;
      }

      public FreemarkerDocumentGenerator buildDomainObject() {
         return new FreemarkerDocumentGenerator(this);
      }
   }

}
