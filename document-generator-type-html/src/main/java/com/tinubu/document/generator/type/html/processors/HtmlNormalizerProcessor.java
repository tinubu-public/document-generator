package com.tinubu.document.generator.type.html.processors;

import static com.tinubu.commons.ddd2.invariant.Invariant.validate;
import static com.tinubu.commons.ddd2.invariant.ParameterValue.value;
import static com.tinubu.commons.ddd2.invariant.rules.MimeTypeRules.isTypeAndSubtypeEqualTo;
import static com.tinubu.commons.lang.mimetype.registry.PresetMimeTypeRegistry.APPLICATION_XHTML;
import static com.tinubu.commons.lang.mimetype.registry.PresetMimeTypeRegistry.TEXT_HTML;
import static com.tinubu.commons.ports.document.domain.rules.DocumentRules.DocumentMainRules.metadata;
import static com.tinubu.commons.ports.document.domain.rules.DocumentRules.DocumentMetadataRules.contentType;
import static java.lang.Math.max;
import static java.nio.charset.StandardCharsets.UTF_8;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Reader;
import java.nio.charset.Charset;
import java.util.concurrent.atomic.AtomicInteger;

import org.w3c.tidy.Tidy;
import org.w3c.tidy.TidyMessage.Level;

import com.tinubu.commons.lang.log.ExtendedLogger;
import com.tinubu.commons.lang.mimetype.registry.PresetMimeTypeRegistry;
import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.document.domain.DocumentAccessException;
import com.tinubu.commons.ports.document.domain.DocumentContent;
import com.tinubu.commons.ports.document.domain.InputStreamDocumentContent;
import com.tinubu.commons.ports.document.domain.InputStreamDocumentContent.InputStreamDocumentContentBuilder;
import com.tinubu.commons.ports.document.domain.LoadedDocumentContent;
import com.tinubu.commons.ports.document.domain.processor.DocumentContentProcessor;
import com.tinubu.commons.ports.document.domain.processor.DocumentProcessor;
import com.tinubu.document.generator.type.html.HtmlDocument;
import com.tinubu.document.generator.type.html.HtmlDocument.HtmlDocumentBuilder;
import com.tinubu.document.generator.type.html.XhtmlDocument;
import com.tinubu.document.generator.type.html.XhtmlDocument.XhtmlDocumentBuilder;
import com.tinubu.document.generator.type.html.printer.TidyHtmlPrinter;

/**
 * HTML normalizer {@link DocumentProcessor} and {@link DocumentContentProcessor}.
 * When used as a {@link #contentProcessor()}, document content-type is not checked and HTML must be
 * parseable.
 * <p>
 * Returned normalized documents, depending on {@link #xhtml} parameter, are of types :
 * <ul>
 *    <li>{@link HtmlDocument}</li>
 *    <li>{@link XhtmlDocument}</li>
 * </ul>
 * The following data are changed in the process :
 * <ul>
 *    <li>document content</li>
 *    <li>metadata content-type</li>
 *    <li>metadata content size</li>
 * </ul>
 */
public class HtmlNormalizerProcessor implements DocumentProcessor, DocumentContentProcessor {

   private static final ExtendedLogger log = ExtendedLogger.of(HtmlNormalizerProcessor.class);

   protected static final boolean DEFAULT_FAIL_ON_ERRORS = false;
   protected static final boolean DEFAULT_FAIL_ON_WARNINGS = false;
   protected static final int MAX_NORMALIZATION_ERRORS = 20;
   protected static final Charset DEFAULT_CONTENT_ENCODING = UTF_8;

   /**
    * Whether to normalize document as XHTML.
    */
   protected final boolean xhtml;
   protected final Charset defaultEncoding;
   protected final boolean failOnWarnings;
   protected final boolean failOnErrors;

   public HtmlNormalizerProcessor(boolean xhtml,
                                  Charset defaultEncoding,
                                  boolean failOnWarnings,
                                  boolean failOnErrors) {
      this.xhtml = xhtml;
      this.defaultEncoding = defaultEncoding;
      this.failOnWarnings = failOnWarnings;
      this.failOnErrors = failOnErrors;
   }

   public HtmlNormalizerProcessor(boolean xhtml, Charset defaultEncoding) {
      this(xhtml, defaultEncoding, DEFAULT_FAIL_ON_WARNINGS, DEFAULT_FAIL_ON_ERRORS);
   }

   public HtmlNormalizerProcessor(boolean xhtml, boolean failOnWarnings, boolean failOnErrors) {
      this(xhtml, DEFAULT_CONTENT_ENCODING, failOnWarnings, failOnErrors);
   }

   public HtmlNormalizerProcessor(boolean xhtml) {
      this(xhtml, DEFAULT_CONTENT_ENCODING, DEFAULT_FAIL_ON_WARNINGS, DEFAULT_FAIL_ON_ERRORS);
   }

   /**
    * Normalizes HTML content
    *
    * @return normalized HTML content
    *
    * @throws DocumentAccessException if a normalization warning or error occurs,
    *       or if an I/O error occurs while reading document content
    */
   @Override
   public DocumentContent process(DocumentContent documentContent) {
      return process(documentContent, defaultEncoding);
   }

   protected DocumentContent process(DocumentContent documentContent, Charset defaultEncoding) {
      Charset templateEncoding = documentContent.contentEncoding().orElse(defaultEncoding);

      AtomicInteger nbWarnings = new AtomicInteger(0);
      AtomicInteger nbErrors = new AtomicInteger(0);

      try (ByteArrayOutputStream tidyErrorStream = new ByteArrayOutputStream();
           PrintWriter tidyErrorWriter = new PrintWriter(tidyErrorStream)) {
         Tidy tidy = new Tidy();

         tidy.setTidyMark(false);
         tidy.setQuiet(true);
         tidy.setShowWarnings(failOnWarnings);
         tidy.setShowErrors(failOnErrors ? max(1, MAX_NORMALIZATION_ERRORS) : MAX_NORMALIZATION_ERRORS);
         tidy.setInputEncoding(templateEncoding.name());
         tidy.setOutputEncoding(templateEncoding.name());
         tidy.setXHTML(xhtml);
         tidy.setErrout(tidyErrorWriter);
         tidy.setMessageListener(message -> {
            if (message.getLevel() == Level.ERROR) {
               nbErrors.incrementAndGet();
            }
            if (message.getLevel() == Level.WARNING) {
               nbWarnings.incrementAndGet();
            }
            if ((message.getLevel() == Level.ERROR && failOnErrors) || (message.getLevel() == Level.WARNING
                                                                        && failOnWarnings)) {
               throw new DocumentAccessException(String.format("Normalization %s : (line %d column %d) %s",
                                                               message.getLevel(),
                                                               message.getLine(),
                                                               message.getColumn(),
                                                               message.getMessage()));
            }
         });

         if (documentContent instanceof InputStreamDocumentContent
             || documentContent instanceof LoadedDocumentContent) {

            try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
               try (Reader contentReader = documentContent.readerContent(templateEncoding)) {
                  org.w3c.dom.Document dom = tidy.parseDOM(contentReader, null);

                  new TidyHtmlPrinter(xhtml, tidy).print(dom, templateEncoding, outputStream);

                  byte[] htmlContent = outputStream.toByteArray();

                  return InputStreamDocumentContentBuilder
                        .from(documentContent)
                        .content(htmlContent, templateEncoding)
                        .build();
               }
            } finally {
               tidyErrorWriter.flush();
               String tidyErrors = tidyErrorStream.toString().trim();
               if (!tidyErrors.isEmpty() && (nbWarnings.get() > 0 || nbErrors.get() > 0)) {
                  log.debug("Normalization : {}", () -> tidyErrors.replaceAll("\n|\r\n|\r", " | "));
               }
            }

         } else {
            throw new IllegalStateException(String.format("Unsupported '%s' content type",
                                                          documentContent.getClass().getName()));
         }

      } catch (IOException e) {
         throw new DocumentAccessException(e);
      }
   }

   /**
    * Creates a new document with normalized HTML content.
    * Source document must be {@link PresetMimeTypeRegistry#TEXT_HTML}, or
    * {@link PresetMimeTypeRegistry#APPLICATION_XHTML}.
    *
    * @return normalized HTML document as {@link HtmlDocument} or {@link XhtmlDocument}
    *
    * @throws DocumentAccessException if a normalization warning or error occurs,
    *       or if an I/O error occurs while reading document content
    */
   @Override
   public Document process(Document document) {
      validate(document,
               "document",
               metadata(contentType(isTypeAndSubtypeEqualTo(value(TEXT_HTML)).orValue(isTypeAndSubtypeEqualTo(
                     value(APPLICATION_XHTML)))))).orThrow();

      Charset documentEncoding = document
            .content()
            .contentEncoding()
            .orElseGet(() -> document.metadata().contentEncoding().orElse(defaultEncoding));

      if (document.content() instanceof InputStreamDocumentContent
          || document.content() instanceof LoadedDocumentContent) {

         if (xhtml) {
            return XhtmlDocumentBuilder
                  .from(document)
                  .metadata(metadata -> metadata.contentType(APPLICATION_XHTML))
                  .build().process(d -> process(d, documentEncoding));
         } else {
            return HtmlDocumentBuilder
                  .from(document)
                  .metadata(metadata -> metadata.contentType(TEXT_HTML))
                  .build().process(d -> process(d, documentEncoding));
         }

      } else {
         throw new IllegalStateException(String.format("Unsupported '%s' content type",
                                                       document.content().getClass().getName()));
      }

   }

   protected Document process(Document document, Charset defaultEncoding) {
      return document.content(process(document.content(), defaultEncoding));
   }

}
