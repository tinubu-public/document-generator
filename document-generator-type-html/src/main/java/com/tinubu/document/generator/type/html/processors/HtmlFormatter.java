package com.tinubu.document.generator.type.html.processors;

import java.nio.charset.Charset;

import org.w3c.dom.Document;

import com.tinubu.document.generator.type.html.printer.HtmlPrinter;

/**
 * Content formatter for HTML documents.
 * When used as a {@link #contentProcessor()}, document content-type is not checked and HTML must be
 * parseable.
 */
public class HtmlFormatter extends HtmlDomProcessor {

   public HtmlFormatter(Charset defaultEncoding, HtmlPrinter printer) {
      super(defaultEncoding, printer);
   }

   public HtmlFormatter(Charset defaultEncoding) {
      super(defaultEncoding);
   }

   public HtmlFormatter(HtmlPrinter printer) {
      super(printer);
   }

   public HtmlFormatter() {
   }

   @Override
   protected void updateDocument(Document document) { }

}
