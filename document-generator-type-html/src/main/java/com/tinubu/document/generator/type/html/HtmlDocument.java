/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.document.generator.type.html;

import static com.tinubu.commons.ddd2.invariant.ParameterValue.value;
import static com.tinubu.commons.ddd2.invariant.rules.MimeTypeRules.isTypeAndSubtypeEqualTo;
import static com.tinubu.commons.lang.mimetype.registry.PresetMimeTypeRegistry.TEXT_HTML;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.util.NullableUtils.nullableInstanceOf;
import static com.tinubu.commons.lang.validation.Validate.notNull;
import static java.nio.charset.StandardCharsets.UTF_8;

import java.io.InputStream;
import java.io.OutputStream;
import java.io.Reader;
import java.io.Writer;
import java.nio.charset.Charset;

import com.tinubu.commons.ddd2.domain.type.Fields;
import com.tinubu.commons.ddd2.invariant.InvariantRule;
import com.tinubu.commons.lang.mimetype.MimeType;
import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.domain.typed.TypedDocument;
import com.tinubu.document.generator.type.html.processors.HtmlNormalizerProcessor;

/**
 * Specialized {@link Document} representing a {@code text/html} document.
 */
public class HtmlDocument extends TypedDocument<HtmlDocument> {

   /**
    * Line separator for {@link #writeLn(String)} operation.
    */
   protected static final String LINE_SEPARATOR = "\n";
   /**
    * Document Content-type.
    */
   protected static final MimeType CONTENT_TYPE = TEXT_HTML;
   /**
    * Default content encoding.
    *
    * @implSpec Default should be deterministic here.
    */
   protected static final Charset DEFAULT_CONTENT_ENCODING = UTF_8;

   protected final boolean normalized;

   protected HtmlDocument(HtmlDocumentBuilder builder) {
      super(builder);
      this.normalized = nullable(builder.normalized, false);
   }

   @Override
   @SuppressWarnings("unchecked")
   public Fields<? extends HtmlDocument> defineDomainFields() {
      return Fields
            .<HtmlDocument>builder()
            .superFields((Fields<HtmlDocument>) super.defineDomainFields())
            .technicalField("normalized", v -> v.normalized)
            .build();
   }

   /**
    * Creates a new html document from existing document. Source document metadata are not changed.
    *
    * @param document existing document to base on
    * @param assumeContentType whether to assume document content-type if content-type is unknown or has
    *       no encoding
    *
    * @return new html document
    */
   public static HtmlDocument of(Document document, boolean assumeContentType) {
      notNull(document, "document");

      return new HtmlDocumentBuilder()
            .reconstitute()
            .copy(document)
            .assumeContentType(assumeContentType, CONTENT_TYPE)
            .build();
   }

   public static HtmlDocument of(Document document) {
      return of(document, false);
   }

   public static HtmlDocument open(DocumentPath documentId,
                                   OutputStream contentOutputStream,
                                   Charset contentEncoding) {
      notNull(documentId, "documentId");

      return new HtmlDocumentBuilder()
            .open(documentId, contentOutputStream, nullable(contentEncoding, DEFAULT_CONTENT_ENCODING),
                  CONTENT_TYPE)
            .build();
   }

   public static HtmlDocument open(DocumentPath documentId, Charset contentEncoding) {
      return open(documentId, null, contentEncoding);
   }

   public static HtmlDocument open(DocumentPath documentId, OutputStream contentOutputStream) {
      return open(documentId, contentOutputStream, null);
   }

   public static HtmlDocument open(DocumentPath documentId) {
      return open(documentId, null, null);
   }

   public HtmlDocument normalize(boolean failOnWarnings, boolean failOnErrors) {
      if (normalized) {
         return this;
      }
      return HtmlDocumentBuilder
            .from(process(new HtmlNormalizerProcessor(false, failOnWarnings, failOnErrors)))
            .normalized(true)
            .build();
   }

   public HtmlDocument normalize() {
      return normalize(false, false);
   }

   /**
    * Returns a XHTML normalized document from this HTML document.
    *
    * @return XHTML normalized document from this HTML document
    */
   public XhtmlDocument xhtml() {
      return XhtmlDocument.of(this);
   }

   @Override
   public HtmlDocument write(byte[] buffer, int offset, int length) {
      return super.write(buffer, offset, length);
   }

   @Override
   public HtmlDocument write(byte[] buffer) {
      return super.write(buffer);
   }

   @Override
   public HtmlDocument write(InputStream inputStream) {
      return super.write(inputStream);
   }

   @Override
   public HtmlDocument write(String string) {
      return super.write(string);
   }

   @Override
   public HtmlDocument write(Reader reader) {
      return super.write(reader);
   }

   @Override
   public HtmlDocument write(Document document) {
      return super.write(document);
   }

   @Override
   public HtmlDocument writeLn(String string) {
      return super.writeLn(string);
   }

   @Override
   public int read(byte[] buffer, int offset, int length) {
      return super.read(buffer, offset, length);
   }

   @Override
   public int read(byte[] buffer) {
      return super.read(buffer);
   }

   @Override
   public int read(OutputStream outputStream) {
      return super.read(outputStream);
   }

   @Override
   public int read(Writer writer) {
      return super.read(writer);
   }

   @Override
   public int read(Document document) {
      return super.read(document);
   }

   @Override
   public String readLn() {
      return super.readLn();
   }

   @Override
   public String lineSeparator() {
      return LINE_SEPARATOR;
   }

   @Override
   public HtmlDocument finish() {
      return super.finish();
   }

   @Override
   protected InvariantRule<MimeType> hasValidContentType() {
      return isTypeAndSubtypeEqualTo(value(CONTENT_TYPE));
   }

   @Override
   public HtmlDocumentBuilder documentBuilder() {
      return new HtmlDocumentBuilder();
   }

   public static HtmlDocumentBuilder reconstituteBuilder() {
      return new HtmlDocumentBuilder().reconstitute();
   }

   public static class HtmlDocumentBuilder extends TypedDocumentBuilder<HtmlDocument, HtmlDocumentBuilder> {

      private Boolean normalized;

      /**
       * Copy constructor for {@link HtmlDocument}
       *
       * @param document document to copy
       *
       * @return html document builder
       */
      public static HtmlDocumentBuilder from(Document document) {
         notNull(document, "document");

         return new HtmlDocumentBuilder().reconstitute().copy(document);
      }

      @Override
      protected HtmlDocumentBuilder copy(Document document) {
         return super
               .copy(document)
               .optionalChain(nullableInstanceOf(document, HtmlDocument.class),
                              (b, d) -> b.normalized(d.normalized));
      }

      protected HtmlDocumentBuilder assumeContentType(boolean assumeContentType, MimeType contentType) {
         return super.assumeContentType(assumeContentType, assumeContentType, contentType);
      }

      public HtmlDocumentBuilder normalized(Boolean normalized) {
         ensureReconstitute();
         this.normalized = normalized;
         return this;
      }

      @Override
      public HtmlDocument buildDomainObject() {
         return new HtmlDocument(this);
      }

   }
}
