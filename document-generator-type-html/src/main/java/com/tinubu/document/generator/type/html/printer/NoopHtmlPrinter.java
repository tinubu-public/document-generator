package com.tinubu.document.generator.type.html.printer;

import java.io.OutputStream;
import java.nio.charset.Charset;

import org.w3c.dom.Document;

/**
 * Noop {@link HtmlPrinter} that does not output anything.
 */
public class NoopHtmlPrinter implements HtmlPrinter {

   public void print(Document document, Charset encoding, OutputStream output) {
   }

}

