package com.tinubu.document.generator.type.html.processors.printer;

import static java.nio.charset.StandardCharsets.UTF_8;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

import org.junit.jupiter.api.Test;

import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.document.domain.DocumentAccessException;
import com.tinubu.document.generator.type.html.printer.TidyHtmlPrinter;
import com.tinubu.document.generator.type.html.processors.HtmlDomProcessor;

public class TidyHtmlPrinterTest extends BaseHtmlPrinterTest {

   @Test
   public void testPrinterWhenNominal() {
      HtmlDomProcessor processor = identityProcessor().printer(new TidyHtmlPrinter(false));

      Document html = htmlDocument(UTF_8).loadContent();
      assertThatExceptionOfType(DocumentAccessException.class)
            .isThrownBy(() -> html.process(processor))
            .withMessage("Document must be an instance of 'org.w3c.tidy.DOMDocumentImpl'");
   }

}