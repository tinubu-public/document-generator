package com.tinubu.document.generator.type.html.processors.printer;

import static java.nio.charset.StandardCharsets.UTF_8;

import java.nio.charset.Charset;

import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.domain.typed.GenericDocument;
import com.tinubu.document.generator.type.html.processors.HtmlDomProcessor;

public abstract class BaseHtmlPrinterTest {

   protected HtmlDomProcessor identityProcessor() {
      return new HtmlDomProcessor() {
         @Override
         protected void updateDocument(org.w3c.dom.Document document) {
         }
      };
   }

   protected Document htmlDocument(Charset encoding) {
      GenericDocument html = GenericDocument.open(DocumentPath.of("test.html"), encoding);

      StringBuilder content = new StringBuilder()
            .append("<!DOCTYPE html>\n")
            .append("<html lang=\"en\">\n")
            .append("<head>\n")
            .append("</head>\n")
            .append("<body>\n")
            .append("</body>\n")
            .append("</html>\n");

      html.write(content.toString().getBytes(UTF_8));
      html.finish();

      return html.readableContent();
   }

}