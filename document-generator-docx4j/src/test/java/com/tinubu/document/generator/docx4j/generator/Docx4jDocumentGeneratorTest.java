package com.tinubu.document.generator.docx4j.generator;

import static com.tinubu.commons.lang.mimetype.MimeTypeFactory.mimeType;
import static com.tinubu.commons.lang.mimetype.registry.PresetMimeTypeRegistry.APPLICATION_OOXML_DOCX;
import static com.tinubu.commons.lang.mimetype.registry.PresetMimeTypeRegistry.APPLICATION_PDF;
import static com.tinubu.commons.lang.mimetype.registry.PresetMimeTypeRegistry.APPLICATION_XHTML;
import static com.tinubu.commons.lang.mimetype.registry.PresetMimeTypeRegistry.TEXT_HTML;
import static com.tinubu.document.generator.core.model.TextModel.TextModelBuilder;
import static com.tinubu.document.generator.core.support.DebugUtils.openDocument;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.time.ZoneId;
import java.util.Locale;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import com.tinubu.commons.ports.document.classpath.ClasspathDocumentRepository;
import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.domain.DocumentRepository;
import com.tinubu.document.generator.core.environment.ConfigurableEnvironment.ConfigurableEnvironmentBuilder;
import com.tinubu.document.generator.core.environment.Environment;
import com.tinubu.document.generator.core.environment.SystemEnvironment;
import com.tinubu.document.generator.core.generator.DocumentGenerator;
import com.tinubu.document.generator.core.generator.GeneratedDocument.GeneratedDocumentBuilder;
import com.tinubu.document.generator.core.skin.CssSkin.CssSkinBuilder;
import com.tinubu.document.generator.docx4j.generator.Docx4jDocumentGenerator.Docx4jDocumentGeneratorBuilder;
import com.tinubu.document.generator.docx4j.skin.DocxSkin;
import com.tinubu.document.generator.docx4j.skin.DocxSkin.DocxSkinBuilder;
import com.tinubu.document.generator.docx4j.template.DocxTemplate.DocxTemplateBuilder;
import com.tinubu.document.generator.type.html.template.XhtmlTemplate.XhtmlTemplateBuilder;

class Docx4jDocumentGeneratorTest {

   @Test
   void generateDocxDocumentWhenDocxTemplate() {
      DocumentGenerator generator =
            new Docx4jDocumentGeneratorBuilder(environment()).outputType(APPLICATION_OOXML_DOCX).build();

      try (var document = generator
            .generateFromTemplate(new DocxTemplateBuilder()
                                        .templateRepository(templateRepository())
                                        .template(DocumentPath.of("template.docx"))
                                        .build(),
                                  new TextModelBuilder().data(dataModel()).build(),
                                  new DocxSkinBuilder()
                                        .skinRepository(skinRepository())
                                        .skin(DocumentPath.of("skin.docx"))
                                        .build())
            .loadDocumentContent()) {

         Document docx = document.document();

         assertThat(docx).isNotNull();
         assertThat(docx.metadata().simpleContentType()).hasValue(APPLICATION_OOXML_DOCX);
         assertThat(docx
                          .metadata()
                          .contentSize()).hasValueSatisfying(contentSize -> assertThat(contentSize).isPositive());
         assertThat(docx.content().content()).isNotEmpty();
      }
   }

   @Test
   void generateDocxDocumentWhenHtmlTemplate() {
      DocumentGenerator generator =
            new Docx4jDocumentGeneratorBuilder(environment()).outputType(APPLICATION_OOXML_DOCX).build();

      try (var document = generator
            .generateFromTemplate(new XhtmlTemplateBuilder()
                                        .templateRepository(templateRepository())
                                        .template(DocumentPath.of("template.html"))
                                        .build(),
                                  new TextModelBuilder().data(dataModel()).build(),
                                  new DocxSkinBuilder()
                                        .skinRepository(skinRepository())
                                        .skin(DocumentPath.of("skin.docx"))
                                        .build())
            .loadDocumentContent()) {

         Document docx = document.document();

         assertThat(docx).isNotNull();
         assertThat(docx.metadata().simpleContentType()).hasValue(APPLICATION_OOXML_DOCX);
         assertThat(docx
                          .metadata()
                          .contentSize()).hasValueSatisfying(contentSize -> assertThat(contentSize).isPositive());
         assertThat(docx.content().content()).isNotEmpty();

         openDocument(docx);
      }
   }

   @Test
   void generateDocxDocumentWhenNoSkin() {
      DocumentGenerator generator =
            new Docx4jDocumentGeneratorBuilder(environment()).outputType(APPLICATION_OOXML_DOCX).build();

      try (var document = generator
            .generateFromTemplate(new DocxTemplateBuilder()
                                        .templateRepository(templateRepository())
                                        .template(DocumentPath.of("template.docx"))
                                        .build(),
                                  new TextModelBuilder().data(dataModel()).build(),
                                  DocxSkin.noopSkin())
            .loadDocumentContent()) {

         Document docx = document.document();

         assertThat(docx).isNotNull();
         assertThat(docx.metadata().simpleContentType()).hasValue(APPLICATION_OOXML_DOCX);
         assertThat(docx
                          .metadata()
                          .contentSize()).hasValueSatisfying(contentSize -> assertThat(contentSize).isPositive());
         assertThat(docx.content().content()).isNotEmpty();

         openDocument(docx);
      }
   }

   @Test
   void generateHtmlDocumentWhenDocxTemplate() {
      new CssSkinBuilder().skinContent("body { color: red; }").build();

      DocumentGenerator generator =
            new Docx4jDocumentGeneratorBuilder(environment()).outputType(TEXT_HTML).build();

      try (var document = generator
            .generateFromTemplate(new DocxTemplateBuilder()
                                        .templateRepository(templateRepository())
                                        .template(DocumentPath.of("template.docx"))
                                        .build(),
                                  new TextModelBuilder().data(dataModel()).build(),
                                  new DocxSkinBuilder()
                                        .skinRepository(skinRepository())
                                        .skin(DocumentPath.of("skin.docx"))
                                        .htmlSkin(new CssSkinBuilder()
                                                        .skinContent("body { color: red; }")
                                                        .build())
                                        .build())
            .loadDocumentContent()) {

         openDocument(document);
      }
   }

   @Test
   void generatePdfDocumentWhenDocxTemplate() {
      DocumentGenerator generator =
            new Docx4jDocumentGeneratorBuilder(environment()).outputType(APPLICATION_PDF).build();

      try (var document = generator
            .generateFromTemplate(new DocxTemplateBuilder()
                                        .templateRepository(templateRepository())
                                        .template(DocumentPath.of("template.docx"))
                                        .build(),
                                  new TextModelBuilder().data(dataModel()).build(),
                                  new DocxSkinBuilder()
                                        .skinRepository(skinRepository())
                                        .skin(DocumentPath.of("skin.docx"))
                                        .build())
            .loadDocumentContent()) {
         openDocument(document);
      }
   }

   @Test
   void convertWhenUnsupportedContentType() {
      DocumentGenerator generator =
            new Docx4jDocumentGeneratorBuilder().outputType(APPLICATION_OOXML_DOCX).build();
      var document = templateRepository().findDocumentById(DocumentPath.of("skin/styles.css")).orElseThrow();
      try (var generatedDocument = new GeneratedDocumentBuilder().document(document).build()) {

         assertThatExceptionOfType(IllegalArgumentException.class)
               .isThrownBy(() -> generator.convert(generatedDocument))
               .withMessage("Unsupported 'skin/styles.css' template");
      }
   }

   @ParameterizedTest
   @ValueSource(strings = { "template.html", "template.docx" })
   void convertWhenOoxmlOutputType(String templateName) {
      DocumentGenerator generator =
            new Docx4jDocumentGeneratorBuilder(environment()).outputType(APPLICATION_OOXML_DOCX).build();
      var document = templateRepository().findDocumentById(DocumentPath.of(templateName)).orElseThrow();
      try (var generatedDocument = new GeneratedDocumentBuilder().document(document).build();
           var convertedDocument = generator.convert(generatedDocument)) {
         var documentMetadata = convertedDocument.document().metadata();

         assertThat(convertedDocument).isNotNull();
         assertThat(documentMetadata.contentType()).hasValue(mimeType(APPLICATION_OOXML_DOCX,
                                                                      StandardCharsets.UTF_8));
      }
   }

   @Test
   void testConvertWhenXhtmlOutputType() {
      DocumentGenerator generator =
            new Docx4jDocumentGeneratorBuilder(environment()).outputType(APPLICATION_XHTML).build();
      var document = templateRepository().findDocumentById(DocumentPath.of("template.html")).orElseThrow();
      try (var generatedDocument = new GeneratedDocumentBuilder().document(document).build();
           var convertedDocument = generator.convert(generatedDocument)) {
         var documentMetadata = convertedDocument.document().metadata();

         assertThat(convertedDocument).isNotNull();
         assertThat(documentMetadata.contentType()).hasValue(mimeType(APPLICATION_XHTML,
                                                                      StandardCharsets.UTF_8));
      }
   }

   private DocumentRepository templateRepository() {
      return new ClasspathDocumentRepository(Path.of("/com/tinubu/document/generator/docx4j/generator"));
   }

   private DocumentRepository skinRepository() {
      return new ClasspathDocumentRepository(Path.of("/com/tinubu/document/generator/docx4j/generator/skin"));
   }

   private Map<String, Object> dataModel() {
      return null;
   }

   private Environment environment() {
      return ConfigurableEnvironmentBuilder
            .ofEnvironment(SystemEnvironment.ofSystemDefaults())
            .timeZone(ZoneId.of("Europe/Paris"))
            .locale(Locale.FRANCE)
            .documentEncoding(StandardCharsets.UTF_8)
            .build();
   }

}