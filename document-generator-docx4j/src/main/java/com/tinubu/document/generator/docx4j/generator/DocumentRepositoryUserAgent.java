package com.tinubu.document.generator.docx4j.generator;

import static com.tinubu.commons.lang.validation.Validate.notNull;

import java.io.InputStream;
import java.io.Reader;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.logging.Level;

import org.docx4j.convert.in.xhtml.renderer.Docx4jUserAgent;

import com.openhtmltopdf.util.LogMessageId;
import com.openhtmltopdf.util.XRLog;
import com.tinubu.commons.ports.document.domain.DocumentPath;
import com.tinubu.commons.ports.document.domain.DocumentRepository;

/**
 * Docx4j {@link Docx4jUserAgent} for {@link DocumentRepository}.
 */
public class DocumentRepositoryUserAgent extends Docx4jUserAgent {

   private final DocumentRepository resourceRepository;

   public DocumentRepositoryUserAgent(DocumentRepository resourceRepository) {
      this.resourceRepository = notNull(resourceRepository, "resourceRepository");
   }

   @Override
   protected InputStream openStream(String uri) {
      try {
         URI result = new URI(uri);
         if (result.getScheme() != null || Paths.get(uri).isAbsolute()) {
            return super.openStream(uri);
         }
      } catch (URISyntaxException e) {
         XRLog.log(Level.WARNING, LogMessageId.LogMessageId1Param.EXCEPTION_MALFORMED_URL, uri, e);
         return null;
      }

      return resourceRepository
            .findDocumentById(DocumentPath.of(uri))
            .map(document -> document.content().inputStreamContent())
            .orElseGet(() -> super.openStream(uri));
   }

   @Override
   protected Reader openReader(String uri) {
      try {
         URI result = new URI(uri);
         if (result.getScheme() != null || Paths.get(uri).isAbsolute()) {
            return super.openReader(uri);
         }
      } catch (URISyntaxException e) {
         XRLog.log(Level.WARNING, LogMessageId.LogMessageId1Param.EXCEPTION_MALFORMED_URL, uri, e);
         return null;
      }

      return resourceRepository
            .findDocumentById(DocumentPath.of(uri))
            .map(document -> document.content().readerContent(StandardCharsets.UTF_8))
            .orElseGet(() -> super.openReader(uri));
   }

   @Override
   public void setBaseURL(String uri) {
      if (uri == null) {
         super.setBaseURL(".");
      } else {
         try {
            URI baseUrl = new URI(uri);
            if (baseUrl.getScheme() != null || Paths.get(uri).isAbsolute()) {
               super.setBaseURL(uri);
            } else {
               super.setBaseURL(normalizeURI(baseUrl).toString());
            }
         } catch (URISyntaxException e) {
            XRLog.log(Level.WARNING, LogMessageId.LogMessageId1Param.EXCEPTION_MALFORMED_URL, uri, e);
         }
      }
   }

   protected String resolveURI(String baseUri, String uri) {
      if (uri == null) {
         return null;
      }

      try {
         URI resolvedUri = new URI(uri);
         URI baseUrl = new URI(baseUri);
         if (baseUrl.getScheme() != null || resolvedUri.getScheme() != null || Paths.get(uri).isAbsolute()) {
            return super.resolveUri(baseUri, uri);
         }

         return normalizePath(Paths
                                    .get(baseUrl.getPath())
                                    .resolve(Paths.get(resolvedUri.getPath()))).toString();
      } catch (URISyntaxException e) {
         XRLog.log(Level.WARNING, LogMessageId.LogMessageId1Param.EXCEPTION_MALFORMED_URL, uri, e);
         return null;
      }
   }

   /**
    * If URI has a scheme (file:, http:, ...), this resolver is totally bypassed, and regular resolver is
    * used.
    * Otherwise, paths are normalized, absolute paths are relativized to root.
    *
    * @param uri A URI, possibly relative.
    *
    * @return resolved URI
    */
   @Override
   public String resolveURI(String uri) {
      return resolveURI(getBaseURL(), uri);
   }

   private Path normalizeURI(URI uri) {
      return normalizePath(Paths.get(uri.getPath()));
   }

   private Path normalizePath(Path path) {
      Path normalizedPath = path.normalize();
      if (normalizedPath.isAbsolute()) {
         normalizedPath = Paths.get("/").relativize(normalizedPath);
      }
      return normalizedPath;
   }

}