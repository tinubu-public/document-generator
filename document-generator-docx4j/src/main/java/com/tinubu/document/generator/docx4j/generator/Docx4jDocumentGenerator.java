package com.tinubu.document.generator.docx4j.generator;

import static com.tinubu.commons.ddd2.invariant.Invariant.validate;
import static com.tinubu.commons.ddd2.invariant.ParameterValue.value;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNull;
import static com.tinubu.commons.ddd2.invariant.rules.ClassRules.isInstanceOf;
import static com.tinubu.commons.ddd2.invariant.rules.EqualsRules.isIn;
import static com.tinubu.commons.ddd2.invariant.rules.MimeTypeRules.withStrippedParameters;
import static com.tinubu.commons.ddd2.invariant.rules.StringRules.isNotBlank;
import static com.tinubu.commons.lang.mimetype.registry.PresetMimeTypeRegistry.APPLICATION_OOXML_DOCX;
import static com.tinubu.commons.lang.mimetype.registry.PresetMimeTypeRegistry.APPLICATION_OOXML_DOTX;
import static com.tinubu.commons.lang.mimetype.registry.PresetMimeTypeRegistry.APPLICATION_PDF;
import static com.tinubu.commons.lang.mimetype.registry.PresetMimeTypeRegistry.APPLICATION_XHTML;
import static com.tinubu.commons.lang.mimetype.registry.PresetMimeTypeRegistry.TEXT_HTML;
import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.document.generator.docx4j.processor.PackageProcessor.identity;
import static java.nio.charset.StandardCharsets.UTF_8;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;

import org.docx4j.Docx4J;
import org.docx4j.Docx4jProperties;
import org.docx4j.convert.in.xhtml.FormattingOption;
import org.docx4j.convert.in.xhtml.XHTMLImporter;
import org.docx4j.convert.in.xhtml.XHTMLImporterImpl;
import org.docx4j.convert.in.xhtml.renderer.DocxRenderer;
import org.docx4j.convert.out.ConversionHTMLStyleElementHandler;
import org.docx4j.convert.out.HTMLSettings;
import org.docx4j.fonts.IdentityPlusMapper;
import org.docx4j.fonts.PhysicalFonts;
import org.docx4j.openpackaging.exceptions.Docx4JException;
import org.docx4j.openpackaging.packages.WordprocessingMLPackage;
import org.w3c.dom.Element;

import com.tinubu.commons.ddd2.domain.type.DomainBuilder;
import com.tinubu.commons.ddd2.domain.type.Fields;
import com.tinubu.commons.lang.beans.Setter;
import com.tinubu.commons.lang.mimetype.MimeType;
import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.document.domain.DocumentRepository;
import com.tinubu.commons.ports.document.domain.InputStreamDocumentContent;
import com.tinubu.commons.ports.document.domain.InputStreamDocumentContent.InputStreamDocumentContentBuilder;
import com.tinubu.commons.ports.document.domain.processor.replacer.PlaceholderReplacer;
import com.tinubu.commons.ports.document.domain.repository.NoopDocumentRepository;
import com.tinubu.commons.ports.document.fs.FsDocumentRepository;
import com.tinubu.document.generator.core.environment.Environment;
import com.tinubu.document.generator.core.environment.SystemEnvironment;
import com.tinubu.document.generator.core.generator.AbstractDocumentGenerator;
import com.tinubu.document.generator.core.generator.DocumentGenerationException;
import com.tinubu.document.generator.core.generator.GeneratedDocument;
import com.tinubu.document.generator.core.generator.GeneratedDocument.GeneratedDocumentBuilder;
import com.tinubu.document.generator.core.model.Model;
import com.tinubu.document.generator.core.model.TextModel;
import com.tinubu.document.generator.core.skin.CssSkin;
import com.tinubu.document.generator.core.skin.Skin;
import com.tinubu.document.generator.core.template.Template;
import com.tinubu.document.generator.docx4j.processor.MergePackageProcessor;
import com.tinubu.document.generator.docx4j.processor.PackageProcessor;
import com.tinubu.document.generator.docx4j.skin.DocxSkin;
import com.tinubu.document.generator.docx4j.template.DocxTemplate;
import com.tinubu.document.generator.docx4j.template.DocxTemplate.DocxTemplateBuilder;
import com.tinubu.document.generator.type.html.template.XhtmlTemplate;
import com.tinubu.document.generator.type.html.template.XhtmlTemplate.XhtmlTemplateBuilder;

// FIXME bookmarks
// FIXME template template/header/footer/watermark ?
// FIXME skin logo ?
// FIXME dotx support ?
// FIXME docm support ?
public class Docx4jDocumentGenerator extends AbstractDocumentGenerator {

   /** Generated content encoding. Docx4j currently seems to always output UTF-8 content. */
   private static final Charset DOCX4J_ENCODING = UTF_8;
   /** Whether to embed images in HTML output. */
   private static final boolean HTML_EMBED_IMAGES = false;
   /** XHTMLImporter max image width. */
   private static final int XHTML_IMPORTER_MAX_WIDTH = 200;
   /**
    * Restrict fonts to specified regex. Set to {@code null} to disable font filtering.
    * e.g.:
    * <ul>
    * <li>Prevent some font loading : {@code (?!.*(?:STIXTwoText|OtherBuggyFont)).*}</li>
    * <li>Restrict fonts on Unix : {@code .*(Courier New|Arial|Times New Roman|Comic Sans|Georgia|Impact|Lucida Console|Lucida Sans Unicode|Palatino Linotype|Tahoma|Trebuchet|Verdana|Symbol|Webdings|Wingdings|MS Sans Serif|MS Serif).*}</li>
    * </ul>
    */
   private static final String FONTS_PATTERN = "(?!.*(?:STIXTwoText)).*";

   private final MimeType outputType;
   private final int documentFlags;
   private final String documentPassword;
   private final PackageProcessor preProcessor;
   private final PackageProcessor postProcessor;

   protected Docx4jDocumentGenerator(Docx4jDocumentGeneratorBuilder builder) {
      super(builder.environment);
      this.outputType = nullable(builder.outputType, APPLICATION_OOXML_DOCX);
      this.documentFlags = nullable(builder.documentFlags, Docx4J.FLAG_SAVE_ZIP_FILE);
      this.documentPassword = builder.documentPassword;
      this.preProcessor = nullable(builder.preProcessor, identity());
      this.postProcessor = nullable(builder.postProcessor, identity());
   }

   @Override
   @SuppressWarnings("unchecked")
   public Fields<? extends Docx4jDocumentGenerator> defineDomainFields() {
      return Fields
            .<Docx4jDocumentGenerator>builder()
            .superFields((Fields<Docx4jDocumentGenerator>) super.defineDomainFields())
            .field("outputType",
                   v -> v.outputType,
                   isNotNull().andValue(withStrippedParameters(isIn(value(list(APPLICATION_OOXML_DOCX,
                                                                               APPLICATION_XHTML,
                                                                               TEXT_HTML,
                                                                               APPLICATION_PDF))))))
            .field("documentFlags", v -> v.documentFlags, isNotNull())
            .field("documentPassword", v -> v.documentPassword, isNull().orValue(isNotBlank()))
            .build();
   }

   @Override
   public GeneratedDocument generateFromTemplate(Template template, Model model, Skin skin) {
      validate(template,
               "template",
               isInstanceOf(value(XhtmlTemplate.class)).orValue(isInstanceOf(value(DocxTemplate.class)))).orThrow();
      validate(model, "model", isInstanceOf(value(TextModel.class))).orThrow();
      validate(skin, "skin", isInstanceOf(value(DocxSkin.class))).orThrow();

      try {
         return generateDocument(template, (TextModel) model, (DocxSkin) skin);
      } catch (Exception e) {
         throw new DocumentGenerationException(e.getMessage(), e);
      } finally {
         template.closeResources();
      }
   }

   @Override
   @SuppressWarnings("resource")
   public GeneratedDocument convert(GeneratedDocument generatedDocument) {
      validate(generatedDocument, "generatedDocument", isNotNull()).orThrow();

      Template template =
            templateOfGeneratedDocument(generatedDocument).orElseThrow(() -> new IllegalArgumentException(
                  String.format("Unsupported '%s' template",
                                generatedDocument.document().documentId().stringValue())));

      try {
         return generateDocument(template, TextModel.noopModel(), DocxSkin.noopSkin());
      } catch (Exception e) {
         throw new DocumentGenerationException(e.getMessage(), e);
      }
   }

   /**
    * Generates an auto-detected template instance from generated document content type.
    *
    * @param document generated document
    *
    * @return built template
    */
   private Optional<Template> templateOfGeneratedDocument(GeneratedDocument document) {
      return document.document().metadata().simpleContentType().flatMap(contentType -> {
         Template template = null;
         if (contentType.equals(APPLICATION_OOXML_DOCX) || contentType.equals(APPLICATION_OOXML_DOTX)) {
            template = DocxTemplateBuilder.ofGeneratedDocument(document).build();
         } else if (contentType.equals(TEXT_HTML) || contentType.equals(APPLICATION_XHTML)) {
            template = XhtmlTemplateBuilder.ofGeneratedDocument(document).build();
         }

         return nullable(template);
      });
   }

   /**
    * Generates DOCX document using Docx4j.
    *
    * @param template template
    * @param skin skin
    *
    * @return Generated DOCX document
    *
    * @throws DocumentGenerationException if an error occurs while generating the document
    */
   private GeneratedDocument generateDocument(Template template, TextModel model, DocxSkin skin) {
      try (DocumentRepository autocloseTemplateRepository = template.templateRepository().orElse(null);
           DocumentRepository autocloseSkinRepository = skin.skinRepository().orElse(null)) {

         WordprocessingMLPackage wpPackage = preProcessor
               .andThen(postProcessor)
               .process(basePackage(template, model, skin), template, model, skin);

         restrictFonts(wpPackage);

         try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
            Path outdirPath = null;
            DocumentRepository outRepository = new NoopDocumentRepository();

            if (outputType.equals(APPLICATION_OOXML_DOCX)) {
               generateDocx(wpPackage, outputStream);
            } else if (outputType.equals(TEXT_HTML)) {
               Docx4jProperties.setProperty("docx4j.Convert.Out.HTML.OutputMethodXML", false);
               outdirPath = generateHtml(wpPackage, outputStream, skin.htmlOutputSkin().orElse(null));
            } else if (outputType.equals(APPLICATION_XHTML)) {
               Docx4jProperties.setProperty("docx4j.Convert.Out.HTML.OutputMethodXML", true);
               outdirPath = generateHtml(wpPackage, outputStream, skin.htmlOutputSkin().orElse(null));
            } else if (outputType.equals(APPLICATION_PDF)) {
               generatePdf(wpPackage, outputStream);
            } else {
               throw new IllegalStateException("Unsupported output type");
            }

            InputStreamDocumentContent content = new InputStreamDocumentContentBuilder()
                  .content(outputStream.toByteArray(), DOCX4J_ENCODING)
                  .build();

            if (outdirPath != null) {
               outRepository =
                     outRepository.stackRepository(FsDocumentRepository.ofFilesystem(outdirPath, true), true);
            }

            return new GeneratedDocumentBuilder()
                  .document(outputDocument(template.template().documentId(), content, outputType))
                  .documentRepository(outRepository)
                  .build();
         }
      } catch (Exception e) {
         throw new IllegalStateException(e);
      }
   }

   private void generatePdf(WordprocessingMLPackage wpPackage, ByteArrayOutputStream outputStream)
         throws Docx4JException {
      Docx4J.toPDF(wpPackage, outputStream);
   }

   private Path generateHtml(WordprocessingMLPackage wpPackage,
                             ByteArrayOutputStream outputStream,
                             CssSkin skin) throws Docx4JException {
      Path outdirPath = null;
      HTMLSettings htmlSettings = Docx4J.createHTMLSettings();

      htmlSettings.setOpcPackage(wpPackage);
      htmlSettings.setStyleElementHandler(cssSkinStyleElementHandler(skin, wpPackage::getUserData));
      if (HTML_EMBED_IMAGES) {
         htmlSettings.setImageDirPath("");
      } else {
         try {
            outdirPath = Files.createTempDirectory("document-generator-docx4j-outdir");

            htmlSettings.setImageDirPath(outdirPath.toAbsolutePath().toString());
         } catch (IOException e) {
            throw new IllegalStateException(e);
         }
      }

      Docx4J.toHTML(htmlSettings, outputStream, Docx4J.FLAG_EXPORT_PREFER_XSL);
      return outdirPath;
   }

   /**
    * Appends skin CSS content to inlined style element.
    *
    * @param skin optional CSS skin, or {@code null}
    * @param attributeAccessor model attributes accessor
    *
    * @return style handler
    *
    * @see "HTMLConversionContext#DEFAULT_STYLE_ELEMENT_HANDLER"
    */
   // FIXME @import CSS from main skin are not inlined
   protected static ConversionHTMLStyleElementHandler cssSkinStyleElementHandler(CssSkin skin,
                                                                                 Function<String, Object> attributeAccessor) {
      return (opcPackage, document, styleDefinition) -> {
         Element ret = null;
         if (styleDefinition != null && !styleDefinition.isEmpty()) {
            ret = document.createElement("style");
            ret.appendChild(document.createComment(styleDefinition));

            String skinCss = nullable(skin).flatMap(Skin::skin)
                  .map(skinDocument -> PlaceholderReplacer
                        .of(attributeAccessor)
                        .process(skinDocument.content())
                        .stringContent(UTF_8))
                  .orElse("");

            ret.appendChild(document.createTextNode("\n"));
            ret.appendChild(document.createComment(skinCss));
         }

         return ret;
      };
   }

   private void generateDocx(WordprocessingMLPackage wpPackage, ByteArrayOutputStream outputStream)
         throws Docx4JException {
      wpPackage.save(outputStream, documentFlags, documentPassword);
   }

   private WordprocessingMLPackage docxPackage(Document template) throws Docx4JException {
      return Docx4J.load(template.content().inputStreamContent());
   }

   private XHTMLImporter xhtmlImporter(WordprocessingMLPackage wordMLPackage,
                                       DocumentRepository documentRepository) {
      XHTMLImporterImpl xhtmlImporter = new XHTMLImporterImpl(wordMLPackage);

      DocumentRepositoryUserAgent documentUserAgent = new DocumentRepositoryUserAgent(documentRepository);
      xhtmlImporter.setRenderer(new DocxRenderer(documentUserAgent));

      xhtmlImporter.setRunFormatting(FormattingOption.IGNORE_CLASS);
      xhtmlImporter.setTableFormatting(FormattingOption.IGNORE_CLASS);
      xhtmlImporter.setParagraphFormatting(FormattingOption.IGNORE_CLASS);
      xhtmlImporter.setMaxWidth(XHTML_IMPORTER_MAX_WIDTH, null);

      return xhtmlImporter;
   }

   /**
    * Creates a base ML package from template and skin if any, or returns an empty package based on default
    * configuration (docx4j.properties).
    *
    * @param skin skin
    *
    * @return baseML package, skinned, or empty
    *
    * @throws Docx4JException if an IO error occurs
    */
   private WordprocessingMLPackage basePackage(Template template, TextModel model, DocxSkin skin)
         throws Docx4JException {
      WordprocessingMLPackage wpPackage;

      if (skin.skin().isPresent()) {
         wpPackage = docxPackage(skin.skin().get());
      } else {
         wpPackage = docxPackage();
      }

      if (template instanceof XhtmlTemplate) {
         XHTMLImporter xhtmlImporter = xhtmlImporter(wpPackage, template.templateRepository().orElse(null));
         List<Object> convertedContent =
               xhtmlImporter.convert(template.template().content().inputStreamContent(), null);
         wpPackage = new MergePackageProcessor(convertedContent).process(wpPackage, template, model, skin);
      } else if (template instanceof DocxTemplate) {
         wpPackage = new MergePackageProcessor(docxPackage(template.template())).process(wpPackage,
                                                                                         template,
                                                                                         model,
                                                                                         skin);
      }

      return wpPackage;
   }

   private WordprocessingMLPackage docxPackage() throws Docx4JException {
      return WordprocessingMLPackage.createPackage();
   }

   /**
    * Set a regex to limit to the common fonts in order to lower memory use or remove faulty fonts.
    *
    * @param wpPackage represents a word document
    */
   private static void restrictFonts(WordprocessingMLPackage wpPackage) throws Exception {
      PhysicalFonts.setRegex(FONTS_PATTERN);
      wpPackage.setFontMapper(new IdentityPlusMapper());
   }

   public static class Docx4jDocumentGeneratorBuilder extends DomainBuilder<Docx4jDocumentGenerator> {
      private final Environment environment;
      private MimeType outputType;
      private Integer documentFlags;
      private String documentPassword;
      private PackageProcessor preProcessor;
      private PackageProcessor postProcessor;

      public Docx4jDocumentGeneratorBuilder(Environment environment) {
         this.environment = validate(environment, "environment", isNotNull()).orThrow();
      }

      public Docx4jDocumentGeneratorBuilder() {
         this(SystemEnvironment.ofSystemDefaults());
      }

      @Setter
      public Docx4jDocumentGeneratorBuilder outputType(MimeType outputType) {
         this.outputType = outputType;
         return this;
      }

      @Setter
      public Docx4jDocumentGeneratorBuilder documentFlags(Integer documentFlags) {
         this.documentFlags = documentFlags;
         return this;
      }

      @Setter
      public Docx4jDocumentGeneratorBuilder documentPassword(String documentPassword) {
         this.documentPassword = documentPassword;
         return this;
      }

      @Setter
      public Docx4jDocumentGeneratorBuilder preProcessor(PackageProcessor preProcessor) {
         this.preProcessor = preProcessor;
         return this;
      }

      @Setter
      public Docx4jDocumentGeneratorBuilder postProcessor(PackageProcessor postProcessor) {
         this.postProcessor = postProcessor;
         return this;
      }

      @Override
      protected Docx4jDocumentGenerator buildDomainObject() {
         return new Docx4jDocumentGenerator(this);
      }

   }

}
