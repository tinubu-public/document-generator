package com.tinubu.document.generator.asciidoctor.generator;

import static com.tinubu.commons.ddd2.invariant.Invariant.validate;
import static com.tinubu.commons.ddd2.invariant.ParameterValue.value;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNull;
import static com.tinubu.commons.ddd2.invariant.rules.ClassRules.isInstanceOf;
import static com.tinubu.commons.ddd2.invariant.rules.EqualsRules.isIn;
import static com.tinubu.commons.ddd2.invariant.rules.MapRules.hasNoNullElements;
import static com.tinubu.commons.ddd2.invariant.rules.StringRules.isNotBlank;
import static com.tinubu.commons.lang.io.PlaceholderReplacerReader.mapModelReplacementFunction;
import static com.tinubu.commons.lang.mimetype.registry.PresetMimeTypeRegistry.APPLICATION_DOCBOOK;
import static com.tinubu.commons.lang.mimetype.registry.PresetMimeTypeRegistry.APPLICATION_PDF;
import static com.tinubu.commons.lang.mimetype.registry.PresetMimeTypeRegistry.TEXT_CSS;
import static com.tinubu.commons.lang.mimetype.registry.PresetMimeTypeRegistry.TEXT_HTML;
import static com.tinubu.commons.lang.util.CollectionUtils.entry;
import static com.tinubu.commons.lang.util.CollectionUtils.immutable;
import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static com.tinubu.commons.lang.util.CollectionUtils.listConcat;
import static com.tinubu.commons.lang.util.CollectionUtils.map;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.util.StreamUtils.entryStream;
import static com.tinubu.commons.lang.util.StreamUtils.stream;
import static com.tinubu.commons.lang.util.StreamUtils.streamConcat;
import static com.tinubu.commons.lang.validation.Validate.notNull;
import static com.tinubu.commons.ports.document.domain.DocumentEntrySpecification.noneDocuments;
import static java.util.Map.Entry;
import static org.asciidoctor.Attributes.STYLESHEET_NAME;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.OffsetDateTime;
import java.time.OffsetTime;
import java.time.Year;
import java.time.YearMonth;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAccessor;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.function.BiFunction;

import org.asciidoctor.Asciidoctor;
import org.asciidoctor.Attributes;
import org.asciidoctor.Options;
import org.asciidoctor.OptionsBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.tinubu.commons.ddd2.domain.type.DomainBuilder;
import com.tinubu.commons.ddd2.domain.type.Fields;
import com.tinubu.commons.ddd2.domain.type.Id;
import com.tinubu.commons.ddd2.domain.type.support.DomainObjectSupport;
import com.tinubu.commons.lang.beans.Setter;
import com.tinubu.commons.lang.mimetype.MimeType;
import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.document.domain.DocumentContent;
import com.tinubu.commons.ports.document.domain.DocumentEntrySpecification;
import com.tinubu.commons.ports.document.domain.DocumentRepository;
import com.tinubu.commons.ports.document.domain.InputStreamDocumentContent.InputStreamDocumentContentBuilder;
import com.tinubu.commons.ports.document.domain.processor.DocumentProcessor;
import com.tinubu.commons.ports.document.domain.processor.replacer.PlaceholderReplacer;
import com.tinubu.commons.ports.document.domain.repository.UnionDocumentRepository;
import com.tinubu.commons.ports.document.fs.FsDocumentRepository;
import com.tinubu.document.generator.asciidoctor.skin.PdfThemeSkin;
import com.tinubu.document.generator.asciidoctor.template.AsciidocTemplate;
import com.tinubu.document.generator.asciidoctor.template.AsciidocTemplate.AsciidocTemplateBuilder;
import com.tinubu.document.generator.core.environment.Environment;
import com.tinubu.document.generator.core.environment.SystemEnvironment;
import com.tinubu.document.generator.core.generator.AbstractDocumentGenerator;
import com.tinubu.document.generator.core.generator.DocumentGenerationException;
import com.tinubu.document.generator.core.generator.GeneratedDocument;
import com.tinubu.document.generator.core.generator.GeneratedDocument.GeneratedDocumentBuilder;
import com.tinubu.document.generator.core.model.Model;
import com.tinubu.document.generator.core.model.TextModel;
import com.tinubu.document.generator.core.model.TextModel.TextModelBuilder;
import com.tinubu.document.generator.core.skin.CssSkin;
import com.tinubu.document.generator.core.skin.NoopSkin;
import com.tinubu.document.generator.core.skin.Skin;
import com.tinubu.document.generator.core.template.Template;

// FIXME decompose Java beans in model to x.y.z... recursively
// FIXME YamlSkin pour les PDF ??
// FIXME logoId, header, footer, watermark ?
// FIXME asciidoctor libraries dynamic configuration ?

/**
 * @apiNote You should set {@code java.awt.headless} property to {@code true} when using diagrams.
 */
public class AsciidoctorDocumentGenerator extends AbstractDocumentGenerator {

   private static final Logger log = LoggerFactory.getLogger(AsciidoctorDocumentGenerator.class);

   /** Asciidoctor libraries to load. */
   private static final List<String> ASCIIDOCTOR_LIBRARIES = list("asciidoctor-pdf", "asciidoctor-diagram");

   /** Options that can't be set by users, and are always filtered out in any configuration mode. */
   private static final List<String> INTERNAL_OPTIONS = list(Options.ATTRIBUTES,
                                                             Options.BACKEND,
                                                             Options.DESTINATION_DIR,
                                                             Options.MKDIRS,
                                                             Options.TO_DIR,
                                                             Options.TO_FILE,
                                                             Options.SOURCE,
                                                             Options.IN_PLACE);
   /** Supported and default backend to use. */
   private static final String DEFAULT_BACKEND = "html5";
   private static final Map<String, MimeType> BACKENDS =
         map(entry("html5", TEXT_HTML), entry("pdf", APPLICATION_PDF), entry("docbook", APPLICATION_DOCBOOK));

   private final String outputType;
   private final String modelAttributesPrefix;
   private final Map<String, Object> attributes;
   private final Map<String, Object> options;
   private final boolean automaticLayout;
   private final DocumentEntrySpecification automaticLayoutDocumentSpecification;

   protected AsciidoctorDocumentGenerator(AsciidoctorDocumentGeneratorBuilder builder) {
      super(builder.environment);
      this.outputType = nullable(builder.outputType, DEFAULT_BACKEND);
      this.modelAttributesPrefix = builder.modelAttributesPrefix;
      this.attributes = immutable(map(builder.attributes));
      this.options = immutable(map(builder.options));
      this.automaticLayout = nullable(builder.automaticLayout, true);
      this.automaticLayoutDocumentSpecification =
            nullable(builder.automaticLayoutDocumentSpecification, noneDocuments());
   }

   @Override
   @SuppressWarnings("unchecked")
   public Fields<? extends AsciidoctorDocumentGenerator> defineDomainFields() {
      return Fields
            .<AsciidoctorDocumentGenerator>builder()
            .superFields((Fields<AsciidoctorDocumentGenerator>) super.defineDomainFields())
            .field("outputType",
                   v -> v.outputType,
                   isNotBlank().andValue(isIn(value(list("html5", "pdf", "docbook")))))
            .field("modelAttributesPrefix", v -> v.modelAttributesPrefix, isNull().orValue(isNotBlank()))
            .field("attributes", v -> v.attributes, hasNoNullElements())
            .field("options", v -> v.options, hasNoNullElements())
            .field("automaticLayout", v -> v.automaticLayout)
            .field("automaticLayoutDocumentSpecification",
                   v -> v.automaticLayoutDocumentSpecification,
                   isNotNull())
            .build();
   }

   @Override
   public GeneratedDocument generateFromTemplate(Template template, Model model, Skin skin) {
      validate(template, "template", isInstanceOf(value(AsciidocTemplate.class))).orThrow();
      validate(model, "model", isInstanceOf(value(TextModel.class))).orThrow();
      if (outputType.equals("html5")) {
         validate(skin, "skin", isInstanceOf(value(CssSkin.class))).orThrow();
      } else if (outputType.equals("pdf")) {
         validate(skin, "skin", isInstanceOf(value(PdfThemeSkin.class))).orThrow();
      } else {
         validate(skin, "skin", isInstanceOf(value(NoopSkin.class))).orThrow();
      }

      try {
         return generateDocument((AsciidocTemplate) template, (TextModel) model, skin);
      } catch (Exception e) {
         throw new DocumentGenerationException(e.getMessage(), e);
      } finally {
         template.closeResources();
      }
   }

   @Override
   @SuppressWarnings("resource")
   public GeneratedDocument convert(GeneratedDocument generatedDocument) {
      validate(generatedDocument, "generatedDocument", isNotNull()).orThrow();

      try {
         return generateDocument(AsciidocTemplateBuilder.ofGeneratedDocument(generatedDocument).build(),
                                 TextModel.noopModel(),
                                 CssSkin.noopSkin());
      } catch (Exception e) {
         throw new DocumentGenerationException(e.getMessage(), e);
      }
   }

   private GeneratedDocument generateDocument(AsciidocTemplate template, TextModel model, Skin skin) {
      try (Asciidoctor asciidoctor = Asciidoctor.Factory.create()) {
         asciidoctor.requireLibraries(ASCIIDOCTOR_LIBRARIES);

         Document templateDocument = template.template();
         Charset documentEncoding = environment().documentEncoding();

         Map<String, Object> attributes = attributes(model, template, skin);

         DocumentRepository baseRepository = baseDocumentRepository(template, skin);
         try (FsDocumentRepository basedirLayout = automaticLayout(baseRepository, skin, attributes)) {
            try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {

               Path outdirPath = outDirectory();

               if (outdirPath != null) {
                  attributes.put("outdir", outdirPath.toAbsolutePath().toString());
                  baseRepository =
                        baseRepository.stackRepositories(FsDocumentRepository.ofFilesystem(outdirPath, true));
               }

               Options options = options(attributes);

               asciidoctor
                     .javaExtensionRegistry()
                     .includeProcessor(new DocumentRepositoryIncludeProcessor(list(), baseRepository,
                                                                              documentEncoding));

               if (basedirLayout != null) {
                  options.setBaseDir(basedirLayout.storagePath().toAbsolutePath().toString());
               }

               options.setToStream(outputStream);
               asciidoctor.convert(templateDocument.content().stringContent(documentEncoding), options);

               DocumentContent content = new InputStreamDocumentContentBuilder()
                     .content(outputStream.toByteArray(), documentEncoding)
                     .build();

               return new GeneratedDocumentBuilder().document(outputDocument(template.template().documentId(),
                                              content,
                                              contentTypeForBackend(outputType)))
                     .documentRepository(baseRepository)
                     .build();
            } catch (IOException e) {
               throw new IllegalStateException(e);
            }
         }
      }
   }

   /**
    * Creates a temporary base directory, transfers automaticLayoutDocumentSpecification, and all known
    * documents to it, excepting template documents, as an include-processor is already able to read them
    * directly.
    * <p>
    * Extra CSS documents are filtered to replace placeholders with model.
    *
    * @param baseRepository source repository to transfer all documents from
    * @param skin skin
    *
    * @return sandbox repository layout, or {@code null} if automatic layout is disabled
    */
   private FsDocumentRepository automaticLayout(DocumentRepository baseRepository,
                                                Skin skin,
                                                Map<String, Object> attributes) {
      if (automaticLayout) {
         try {
            Path layoutPath = Files.createTempDirectory("document-generator-asciidoctor-layout");

            log.debug("[Layout] layout directory : {}", layoutPath);

            FsDocumentRepository layoutRepository = createSandboxRepository(layoutPath,
                                                                            baseRepository,
                                                                            automaticLayoutDocumentSpecification,
                                                                            cssSimpleReplacer(attributes));

            try {
               streamConcat(stream(skin.logoId()),
                            stream(skin.watermarkId())).forEach(skinDocument -> baseRepository.transferDocument(
                     skinDocument,
                     layoutRepository,
                     false));

               streamConcat(stream(skin.skin())).forEach(skinDocument -> layoutRepository.saveDocument(
                     skinDocument.process(cssSimpleReplacer(attributes)),
                     false));

            } catch (Exception e) {
               layoutRepository.close();
               throw e;
            }

            return layoutRepository;
         } catch (IOException e) {
            throw new IllegalStateException(e);
         }
      }

      return null;
   }

   private DocumentProcessor cssSimpleReplacer(Map<String, Object> attributes) {
      return document -> {
         if (stream(TEXT_CSS).anyMatch(replaceContentType -> document
               .metadata()
               .simpleContentType()
               .map(replaceContentType::matches)
               .orElse(false))) {
            return document.process(PlaceholderReplacer.of(mapModelReplacementFunction(attributes)));
         } else {
            return document;
         }
      };
   }

   private Path outDirectory() {
      Path outdirPath = null;

      if (automaticLayout) {
         try {
            outdirPath = Files.createTempDirectory("document-generator-asciidoctor-outdir");

            log.debug("[Layout] output directory : {}", outdirPath);

            return outdirPath;
         } catch (IOException e) {
            throw new IllegalStateException(e);
         }
      }

      return outdirPath;
   }

   private DocumentRepository baseDocumentRepository(Template template, Skin skin) {
      return UnionDocumentRepository.ofLayers(listConcat(stream(template.templateRepository()),
                                                         stream(skin.skinRepository())));
   }

   private Options options(Map<String, Object> attributes) {
      OptionsBuilder options = Options.builder();

      automaticOptions(options);
      userOptions(options);

      options.backend(outputType);
      options.attributes(Attributes.builder().attributes(attributes).build());

      return options.build();
   }

   private void automaticOptions(OptionsBuilder options) {
      options.headerFooter(true);
   }

   private void userOptions(OptionsBuilder options) {
      entryStream(this.options)
            .filter(option -> !INTERNAL_OPTIONS.contains(option.getKey()))
            .filter(option -> !option.getKey().equals(Options.BASEDIR) || !automaticLayout)
            .forEach(option -> options.option(option.getKey(), option.getValue()));
   }

   private Map<String, Object> attributes(TextModel model, AsciidocTemplate template, Skin skin) {
      Map<String, Object> automaticAttributes = new LinkedHashMap<>();

      automaticAttributes.putAll(modelAttributes(model, template, skin));
      automaticAttributes.putAll(automaticAttributes(skin));
      automaticAttributes.putAll(userAttributes());

      return automaticAttributes;
   }

   /**
    * Defines some automatic attributes.
    *
    * @param skin skin
    *
    * @return attributes map
    *
    * @implSpec attributes values should be suffixed with {@code @} so that attributes can be overridden
    *       in asciidoc document.
    */
   private Map<String, Object> automaticAttributes(Skin skin) {
      Map<String, Object> automaticAttributes = new LinkedHashMap<>();

      if (skin instanceof CssSkin) {
         skin
               .skin()
               .ifPresent(skinDocument -> automaticAttributes.put(STYLESHEET_NAME,
                                                                  skinDocument.documentId().stringValue()));
      } else if (skin instanceof PdfThemeSkin) {
         automaticAttributes.putAll(((PdfThemeSkin) skin).automaticAttributes());
      }

      return overridableValues(automaticAttributes);
   }

   /**
    * Appends a {@code @} suffix to {@link String} attribute value so that they can be overridden in AsciiDoc
    * document.
    */
   private Object overridableValue(Object attributeValue) {
      if (attributeValue instanceof String) {
         return attributeValue + "@";
      } else {
         return attributeValue;
      }
   }

   private Map<String, Object> overridableValues(Map<String, Object> attributes) {
      return map(LinkedHashMap::new,
                 entryStream(attributes).map(e -> entry(e.getKey(), overridableValue(e.getValue()))));
   }

   private Map<String, Object> userAttributes() {
      return this.attributes;
   }

   private Map<String, Object> modelAttributes(TextModel model, AsciidocTemplate template, Skin skin) {
      Map<String, Object> modelAttributes = addDocumentReferencesToModel(model, template, skin).data();

      if (modelAttributesPrefix != null) {
         modelAttributes = map(entryStream(modelAttributes).map(e -> entry(modelAttributesPrefix + e.getKey(),
                                                                           modelAttributeValue(e.getValue()))));
      }

      return modelAttributes;
   }

   private String modelAttributeValue(Object modelValue) {
      if (modelValue instanceof ZonedDateTime) {
         return DateTimeFormatter
               .ofPattern(environment().dateTimeFormat(), environment().locale())
               .format(((ZonedDateTime) modelValue));
      } else if (modelValue instanceof OffsetDateTime) {
         return DateTimeFormatter
               .ofPattern(environment().dateTimeFormat(), environment().locale())
               .format(((OffsetDateTime) modelValue).atZoneSameInstant(environment().timeZone()));
      } else if (modelValue instanceof LocalDateTime) {
         return DateTimeFormatter
               .ofPattern(environment().dateTimeFormat(), environment().locale())
               .format(((LocalDateTime) modelValue));
      } else if (modelValue instanceof LocalDate
                 || modelValue instanceof YearMonth
                 || modelValue instanceof Year) {
         return DateTimeFormatter
               .ofPattern(environment().dateFormat(), environment().locale())
               .format((TemporalAccessor) modelValue);
      } else if (modelValue instanceof LocalTime || modelValue instanceof OffsetTime) {
         return DateTimeFormatter
               .ofPattern(environment().timeFormat(), environment().locale())
               .format((TemporalAccessor) modelValue);
      } else if (modelValue instanceof Instant) {
         return DateTimeFormatter
               .ofPattern(environment().dateTimeFormat(), environment().locale())
               .format(OffsetDateTime.ofInstant((Instant) modelValue, environment().timeZone()));
      } else {
         return String.valueOf(modelValue);
      }

   }

   private TextModel addDocumentReferencesToModel(TextModel model, AsciidocTemplate template, Skin skin) {
      return TextModelBuilder
            .from(model)
            .addData("templateId", template.template().documentId())
            .optionalChain(template.header(), addDocument("headerId"))
            .optionalChain(template.footer(), addDocument("footerId"))
            .optionalChain(skin.skin(), addDocument("skinId"))
            .optionalChain(skin.logoId(), addId("logoId"))
            .optionalChain(skin.watermarkId(), addId("watermarkId"))
            .build();
   }

   private <U> BiFunction<TextModelBuilder, U, TextModelBuilder> addData(String dataKey) {
      return (builder, object) -> builder.addData(dataKey, object);
   }

   private <ID extends Id> BiFunction<TextModelBuilder, ID, TextModelBuilder> addId(String dataKey) {
      return (builder, id) -> builder.addData(dataKey, id.stringValue());
   }

   private <D extends Document> BiFunction<TextModelBuilder, D, TextModelBuilder> addDocument(String dataKey) {
      return (builder, document) -> builder.addData(dataKey, document.documentId().stringValue());
   }

   private static String backendForContentType(MimeType outputType) {
      notNull(outputType, "outputType");

      return BACKENDS
            .entrySet()
            .stream()
            .filter(entry -> outputType.equals(entry.getValue()))
            .map(Entry::getKey)
            .findAny()
            .orElseThrow(() -> new IllegalStateException(String.format("Unsupported '%s' output content type",
                                                                       outputType)));
   }

   private static MimeType contentTypeForBackend(String backend) {
      notNull(backend, "backend");

      return nullable(BACKENDS.get(backend)).orElseThrow(() -> new IllegalStateException(String.format(
            "Unsupported '%s' backend",
            backend)));
   }

   public static class AsciidoctorDocumentGeneratorBuilder
         extends DomainBuilder<AsciidoctorDocumentGenerator> {
      private final Environment environment;
      private String outputType;
      private Boolean automaticLayout;
      private DocumentEntrySpecification automaticLayoutDocumentSpecification;
      private String modelAttributesPrefix;
      private Map<String, Object> attributes = map();
      private Map<String, Object> options = map();

      public AsciidoctorDocumentGeneratorBuilder(Environment environment) {
         this.environment = validate(environment, "environment", isNotNull()).orThrow();
      }

      public AsciidoctorDocumentGeneratorBuilder() {
         this(SystemEnvironment.ofSystemDefaults());
      }

      @Setter
      public AsciidoctorDocumentGeneratorBuilder outputType(String outputType) {
         this.outputType = outputType;
         return this;
      }

      @Setter
      public AsciidoctorDocumentGeneratorBuilder outputType(MimeType outputType) {
         this.outputType = backendForContentType(notNull(outputType, "outputType"));
         return this;
      }

      @Setter
      public AsciidoctorDocumentGeneratorBuilder automaticLayout(Boolean automaticLayout) {
         this.automaticLayout = automaticLayout;
         return this;
      }

      @Setter
      public AsciidoctorDocumentGeneratorBuilder automaticLayoutDocumentSpecification(
            DocumentEntrySpecification automaticLayoutDocumentSpecification) {
         this.automaticLayoutDocumentSpecification = automaticLayoutDocumentSpecification;
         return this;
      }

      @Setter
      public AsciidoctorDocumentGeneratorBuilder modelAttributesPrefix(String modelAttributesPrefix) {
         this.modelAttributesPrefix = modelAttributesPrefix;
         return this;
      }

      @Setter
      public AsciidoctorDocumentGeneratorBuilder attributes(Map<String, Object> attributes) {
         this.attributes = map(attributes);
         return this;
      }

      public AsciidoctorDocumentGeneratorBuilder attribute(String attribute, Object value) {
         this.attributes.put(attribute, value);
         return this;
      }

      @Setter
      public AsciidoctorDocumentGeneratorBuilder options(Map<String, Object> options) {
         this.options = map(options);
         return this;
      }

      public AsciidoctorDocumentGeneratorBuilder option(String option, Object value) {
         this.options.put(option, value);
         return this;
      }

      @Override
      protected AsciidoctorDocumentGenerator buildDomainObject() {
         return new AsciidoctorDocumentGenerator(this);
      }

      public AsciidoctorDocumentGenerator build() {
         return DomainObjectSupport.build(this::buildDomainObject);
      }
   }

}
